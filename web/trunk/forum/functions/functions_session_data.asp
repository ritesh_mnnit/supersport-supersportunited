<%
'****************************************************************************************
'**  Copyright Notice    
'**
'**  Web Wiz Forums(TM)
'**  http://www.webwizforums.com
'**                            
'**  Copyright (C)2001-2008 Web Wiz(TM). All Rights Reserved.
'**  
'**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS UNDER LICENSE FROM 'WEB WIZ'.
'**  
'**  IF YOU DO NOT AGREE TO THE LICENSE AGREEMENT THEN 'WEB WIZ' IS UNWILLING TO LICENSE 
'**  THE SOFTWARE TO YOU, AND YOU SHOULD DESTROY ALL COPIES YOU HOLD OF 'WEB WIZ' SOFTWARE
'**  AND DERIVATIVE WORKS IMMEDIATELY.
'**  
'**  If you have not received a copy of the license with this work then a copy of the latest
'**  license contract can be found at:-
'**
'**  http://www.webwizguide.com/license
'**
'**  For more information about this software and for licensing information please contact
'**  'Web Wiz' at the address and website below:-
'**
'**  Web Wiz, Unit 10E, Dawkins Road Industrial Estate, Poole, Dorset, BH15 4JD, England
'**  http://www.webwizguide.com
'**
'**  Removal or modification of this copyright notice will violate the license contract.
'**
'****************************************************************************************



'*************************** SOFTWARE AND CODE MODIFICATIONS **************************** 
'**
'** MODIFICATION OF THE FREE EDITIONS OF THIS SOFTWARE IS A VIOLATION OF THE LICENSE  
'** AGREEMENT AND IS STRICTLY PROHIBITED
'**
'** If you wish to modify any part of this software a license must be purchased
'**
'****************************************************************************************







'******************************************
'***  Read in session application data	***
'******************************************

'Sub procedure to read in the session data from application variable or create new one if it doesn't exist
Private Sub getSessionData()

	Dim intSessionArrayPass
	Dim intRemovedEntries
	Dim strNewSessionID
	Dim intSessionStringLength
	Dim blnCookiesDetected
	Dim blnFoundSession
	Dim strIP
	Dim strDate


	'Initialise  variables
	intSessionStringLength = 32
	intRemovedEntries = 0
	blnCookiesDetected = false
	blnFoundSession = false
	strNewSessionID = LCase(hexValue(intSessionStringLength))
	strIP = getIP()
	strQsSID = ""
	strQsSID1 = ""
	strQsSID2 = ""
	strQsSID3 = ""


	'Use only the first 2 parts of the IP address to prevent errors when using mutiple proxies (eg. AOL uses)
	strIP = Mid(strIP, 1, (InStr(InStr(1, strIP, ".", 1)+1, strIP, ".", 1)))


	'Read in the session ID, if available
	'Use cookies first
	If getCookie("sID", "SID") <> "" Then
		'Set the cookie detection variable to true
		blnCookiesDetected = true

		'Get the session ID from cookie
		strSessionID = Trim(getCookie("sID", "SID"))

	'Else if no cookies, or cookies not working use querystrings
	ElseIf Request.QueryString("SID") <> "" Then

		'Get the session ID from querystring
		strSessionID = Trim(Request("SID"))
	End If


	'Check the length of the session ID is correct, if not destroy it
	If Len(strSessionID) <> intSessionStringLength Then strSessionID = ""



	'Session array lookup table
	'0 = Session ID
	'1 = IP address
	'2 = Time last accessed
	'3 = Session data
	
	

	'Read in the session array from the application variable
	If blnDatabaseHeldSessions = false Then
		If isArray(Application(strAppPrefix & "sarySessionData")) Then
			sarySessionData = Application(strAppPrefix & "sarySessionData")
	
		'Else create an array
		Else
			ReDim sarySessionData(3,0)
		End If
	End If
	
	
	
	
	'Read in the session data from the database (worse performance, but required for web gardens and load balanced servers)
	If blnDatabaseHeldSessions Then
		
		'Get all sssion data from database
		strSQL = "SELECT " & strDbTable & "Session.Session_ID, " & strDbTable & "Session.IP_address, " & strDbTable & "Session.Last_active, " & strDbTable & "Session.Session_data " & _
		"FROM " & strDbTable & "Session" & strDBNoLock & ";"
		
		'Set error trapping
		On Error Resume Next
	
		'Get recordset
		rsCommon.Open strSQL, adoCon
				
		'If an error has occurred write an error to the page
		If Err.Number <> 0 Then Call errorMsg("An error has occurred while executing SQL query on database.", "getSessionData()_get_session_data", "functions_session_data.asp")
			
		'Disable error trapping
		On Error goto 0'Open database
		
		
		'If records returned place them in the array
		If NOT rsCommon.EOF Then
			sarySessionData = rsCommon.GetRows()
		'Else create an array
		Else
			ReDim sarySessionData(3,0)
		End If
		
		'Close recordset
		rsCommon.Close
	End If





	'Iterate through array
	For intSessionArrayPass = 0 To UBound(sarySessionData, 2)

		'Check that the array position is not over 20 minutes old and remove them
		If CDate(sarySessionData(2, intSessionArrayPass)) < DateAdd("n", -20, Now()) Then
			
			'First remove any unread post arrays that maybe stored in the memory for this session
			Application("sarryUnReadPosts" &  sarySessionData(0, intSessionArrayPass)) = ""

			'Swap this array postion with the last in the array
			sarySessionData(0, intSessionArrayPass) = sarySessionData(0, UBound(sarySessionData, 2) - intRemovedEntries)
			sarySessionData(1, intSessionArrayPass) = sarySessionData(1, UBound(sarySessionData, 2) - intRemovedEntries)
			sarySessionData(2, intSessionArrayPass) = sarySessionData(2, UBound(sarySessionData, 2) - intRemovedEntries)
			sarySessionData(3, intSessionArrayPass) = sarySessionData(3, UBound(sarySessionData, 2) - intRemovedEntries)

			'Increment the number of removed entries
			intRemovedEntries = intRemovedEntries + 1

		'ElseIf user has a session, read in data, and update last access time (for security we also check the IP address)
		ElseIf sarySessionData(0, intSessionArrayPass) = strSessionID AND sarySessionData(1, intSessionArrayPass) = strIP Then

			'If using a database for session data we need to update the last access time in the database
			'Only update if older than 5 minutes to cut down on database hits (this date is also updated when saving session data, so may not be required in some instances)
			If blnDatabaseHeldSessions AND CDate(sarySessionData(2, intSessionArrayPass)) < DateAdd("n", -5, Now()) Then
			
				'Initilse sql statement
			 	strSQL = "UPDATE " & strDbTable & "Session" & strRowLock & " " & _
				"SET " & strDbTable & "Session.Last_active = " & strDatabaseDateFunction & " " & _
				"WHERE " & strDbTable & "Session.Session_ID = '" & sarySessionData(0, intSessionArrayPass) & "';"
				
				'Set error trapping
				On Error Resume Next
	
				'Write to database
				adoCon.Execute(strSQL)
				
				'If an error has occurred write an error to the page
				If Err.Number <> 0 Then	Call errorMsg("An error has occurred while writing to the database.", "getSessionData()_update_last_active_date", "functions_session_data.asp")
			
				'Disable error trapping
				On Error goto 0
				
			End If
			
			'Set blnFoundSession to true
			blnFoundSession = true

			'Read in session data
			strSessionData = sarySessionData(3, intSessionArrayPass)
			
			
			'Update last access time
			sarySessionData(2, intSessionArrayPass) = internationalDateTime(Now())
		End If

		'If the session ID already exists create a new one
		If strNewSessionID = sarySessionData(0, intSessionArrayPass) Then
			strNewSessionID = LCase(hexValue(intSessionStringLength))
			intSessionArrayPass = 1
		End If
	Next






	'Remove the last array position as it is no-longer needed
	If intRemovedEntries > 0 Then ReDim Preserve sarySessionData(3, UBound(sarySessionData, 2) - intRemovedEntries)
		
	
	'If using a database to store the data then delete old entries from the database
	If blnDatabaseHeldSessions Then
		
		'SQL to delete old entries from the database
		strSQL = "DELETE FROM " & strDbTable & "Session" & strRowLock & " " & _
		"WHERE " & strDbTable & "Session.Last_active < " 
		'If Access use # around dates, other DB's use ' around dates
		If strDatabaseType = "Access" Then
			strSQL = strSQL & "#" & internationalDateTime(DateAdd("n", -20, Now())) & "#;"
		ElseIf strDatabaseType = "SQLServer" Then
			strDate = internationalDateTime(DateAdd("n", -20, Now()))
			strDate = Replace(strDate, "-", "", 1, -1, 1)
			strSQL = strSQL & "'" & strDate & "';"
		Else
			strSQL = strSQL & "'" & internationalDateTime(DateAdd("n", -20, Now())) & "';"
		End If

		'Set error trapping
		On Error Resume Next

		'Execute SQL
		adoCon.Execute(strSQL)
			
		'If an error has occurred write an error to the page
		If Err.Number <> 0 Then	Call errorMsg("An error has occurred while writing to the database.", "getSessionData()_delete_session_data", "functions_session_data.asp")
		
		'Disable error trapping
		On Error goto 0 
	End If





	'If the user doesn't have a session create one
	If blnFoundSession = false Then

		'ReDimesion the array
		ReDim Preserve sarySessionData(3, UBound(sarySessionData, 2) + 1)

		'Update the new array position with the new session details
		sarySessionData(0, UBound(sarySessionData, 2)) = strNewSessionID
		sarySessionData(1, UBound(sarySessionData, 2)) = strIP
		sarySessionData(2, UBound(sarySessionData, 2)) = internationalDateTime(Now())
		sarySessionData(3, UBound(sarySessionData, 2)) = ";"

		'Initilise the session id variable
		strSessionID = strNewSessionID

		'Create a cookie and querystring with the session ID
		Call setCookie("sID", "SID", strNewSessionID, False)
		
		
		'If using a database for session data then save the new session to the database
		If blnDatabaseHeldSessions Then
		
			'SQL to update the database with the new session
		 	strSQL = "INSERT INTO " & strDbTable & "Session (" &_
			"Session_ID, " & _
			"IP_address, " & _
			"Last_active, " & _
			"Session_data " & _
			") " & _
			"VALUES " & _
			"('" & strNewSessionID & "', " & _
			"'" & strIP & "', " & _
			strDatabaseDateFunction & ", " & _
			"';' " & _
			");"
			
			'Set error trapping
			On Error Resume Next

			'Write to database
			adoCon.Execute(strSQL)
			
			'If an error has occurred write an error to the page
			If Err.Number <> 0 Then	Call errorMsg("An error has occurred while writing to the database.", "getSessionData()_save_new_session_data", "functions_session_data.asp")
		
			'Disable error trapping
			On Error goto 0
		
		End If
	End If




	'If cookies are not detected setup to use querystrings to pass around the session ID
	'For better Search Engine indexing don't use Session Querystring if detected as Search Robot
	If blnCookiesDetected = false Then
		strQsSID = strSessionID			'For form entries etc.
		strQsSID1 = "?SID=" & strSessionID	'For ? querystrings
		strQsSID2 = "&amp;SID=" & strSessionID	'For &amp; querystrings
		strQsSID3 = "&SID=" & strSessionID	'For & querystrings - redirects
	End If





	'Update the session application array (if storing sessions in application level array)
	If blnDatabaseHeldSessions = false AND NOT OSType = "Search Robot" Then
		
		'Lock the application so that no other user can try and update the application level variable at the same time
		Application.Lock
	
		'Update the application level variable
		Application(strAppPrefix & "sarySessionData") = sarySessionData
	
		'Unlock the application
		Application.UnLock
	End If
End Sub







'******************************************
'***  Get application session data	***
'******************************************

'Function to read in application session data for those without cookies
Private Function getSessionItem(ByVal strSessionKey)

	Dim saryUserSessionData
	Dim intSessionArrayPass
	
	'Append '=' to the end of the session key to make full session key (eg. key=)
	strSessionKey = strSessionKey & "="

	'Split the session data up into an array
	saryUserSessionData = Split(strSessionData, ";")

	'Loop through array to get the required data
	For intSessionArrayPass = 0 to UBound(saryUserSessionData)
		If InStr(saryUserSessionData(intSessionArrayPass), strSessionKey) Then
			'Return the data item
			getSessionItem = Replace(saryUserSessionData(intSessionArrayPass), strSessionKey, "", 1, -1, 1)
		End If
	Next

End Function







'******************************************
'***  Save application session data	***
'******************************************

'Sub procedure to save application session data for those without cookies
Private Sub saveSessionItem(ByRef strSessionKey, ByRef strSessionKeyValue)

	Dim saryUserSessionData
	Dim intSessionArrayPass
	Dim strNewSessionData
	Dim intItemArrayPass

	'Don't run code if a search engine spider
	If NOT OSType = "Search Robot" Then
		
		'Read in the application session for the user and update the session data in it
		For intSessionArrayPass = 0 To UBound(sarySessionData, 2)
	
			'If we find the users session data update it
			If sarySessionData(0, intSessionArrayPass) = strSessionID Then
	
				'Split the session data up into an array
				saryUserSessionData = Split(sarySessionData(3, intSessionArrayPass), ";")
	
				'Loop through array and do NOT add the updated key to session data
				For intItemArrayPass = 0 to UBound(saryUserSessionData)
	
					If InStr(saryUserSessionData(intItemArrayPass), strSessionKey) = 0 AND saryUserSessionData(intItemArrayPass) <> "" Then
	
						'Create session data string
						strNewSessionData = strNewSessionData & saryUserSessionData(intItemArrayPass) & ";"
					End If
				Next
	
				'Add the updated or new key to session string
				If strSessionKeyValue <> "" Then strNewSessionData = strNewSessionData & strSessionKey & "=" & strSessionKeyValue
	
				'Update the array
				sarySessionData(3, intSessionArrayPass) = ";" & strNewSessionData
				
				
				'If using a database save the session data to database
				If blnDatabaseHeldSessions AND NOT OSType = "Search Robot" Then
					
					'Make sure session data is SQL safe to prevent SQL injections
					strNewSessionData = ";" & formatSQLInput(strNewSessionData)

					'Initilse sql statement
				 	strSQL = "UPDATE " & strDbTable & "Session" & strRowLock & " " & _
					"SET " & strDbTable & "Session.Last_active = " & strDatabaseDateFunction & ", " & strDbTable & "Session.Session_data = '" & strNewSessionData & "' " & _
					"WHERE " & strDbTable & "Session.Session_ID = '" & strSessionID & "';"
					
					'Set error trapping
					On Error Resume Next
		
					'Write to database
					adoCon.Execute(strSQL)
					
					'If an error has occurred write an error to the page
					If Err.Number <> 0 Then	Call errorMsg("An error has occurred while writing to the database.", "saveSessionItem()_update_session_data", "functions_session_data.asp")
				
					'Disable error trapping
					On Error goto 0
					
				
				'Else save the sesison data to the application array
				Else
	
					'Lock the application so that no other user can try and update the application level variable at the same time
					Application.Lock
		
					'Update the application level session data for the user
					Application(strAppPrefix & "sarySessionData") = sarySessionData
		
					'Unlock the application
					Application.UnLock
				End If
	
				'Exit for loop
				Exit For
			End If
		Next
	End If
End Sub


%>