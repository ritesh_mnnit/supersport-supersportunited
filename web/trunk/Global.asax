﻿<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_beginRequest(ByVal sender As Object, ByVal e As EventArgs)
        
        Dim fullOrigionalpath As String
        fullOrigionalpath = Request.RawUrl
        
        ' Mobile check
        Dim UserAgent As String
        UserAgent = ""
        
        If Not Request.ServerVariables("HTTP_USER_AGENT") = Nothing Then
            
            UserAgent = Request.ServerVariables("HTTP_USER_AGENT").Trim().ToLower()
            
        End If
        
        Dim Mobile As Boolean
        
        Mobile = IsMobileBrowser(UserAgent)
        
        If Not Request.QueryString("mobile") = Nothing Then
            Mobile = True
        End If
        
        If Mobile = True Then
            If Not fullOrigionalpath = Nothing Then
                Dim tmpUrl As String
                tmpUrl = "http://mobi.sufc.co.za/" + fullOrigionalpath
                Response.Redirect(tmpUrl)
            Else
                Response.Redirect("http://mobi.sufc.co.za/")
            End If
        End If
        
    End Sub
    
    Function IsMobileBrowser(ByVal UserAgent As String) As Boolean
        Dim Mobile As Boolean
        Mobile = False
        
        Dim Mobiles As New ArrayList
        
        Mobiles.Add("cldc")
        Mobiles.Add("midp")
        Mobiles.Add("symbian")
        Mobiles.Add("up.link")
        Mobiles.Add("windows ce")
        Mobiles.Add("iphone")
        Mobiles.Add("mobile safari")
        
        Dim iEnum As IEnumerator
        iEnum = Mobiles.GetEnumerator()
        
        While iEnum.MoveNext()
            If UserAgent.IndexOf(iEnum.Current) >= 0 Then
                Mobile = True
            End If                 
        End While
        
        Return Mobile
        
    End Function
      
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
</script>


