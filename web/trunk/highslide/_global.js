// JavaScript Document
////////////////////////////////////// results box
function showResults(theNum){
document.getElementById('r_results').style.display = "none";
document.getElementById('r_fixtures').style.display = "none";
document.getElementById('r_standings').style.display = "none";
document.getElementById('result_1').className = "r1on";
document.getElementById('result_2').className = "r2on";
document.getElementById('result_3').className = "r3on";
switch(theNum){
case 1:
document.getElementById('r_results').style.display = "inline";
document.getElementById('result_1').className = "r1off";
break;
case 2:
document.getElementById('r_fixtures').style.display = "inline";
document.getElementById('result_2').className = "r2off";
break;
case 3:
document.getElementById('r_standings').style.display = "inline";
document.getElementById('result_3').className = "r3off";
break;
}
}
////////////////////////////////////// end results box
////////////////////////////////////// eds box
function showEds(theNum){
document.getElementById('r_headlines').style.display = "none";
document.getElementById('r_mostread').style.display = "none";
document.getElementById('r_edschoice').style.display = "none";
document.getElementById('eds_1').className = "r1on";
document.getElementById('eds_2').className = "r2on";
document.getElementById('eds_3').className = "r3on";
switch(theNum){
case 1:
document.getElementById('r_headlines').style.display = "inline";
document.getElementById('eds_1').className = "r1off";
break;
case 2:
document.getElementById('r_mostread').style.display = "inline";
document.getElementById('eds_2').className = "r2off";
break;
case 3:
document.getElementById('r_edschoice').style.display = "inline";
document.getElementById('eds_3').className = "r3off";
break;
}
}
////////////////////////////////////// end eds box
////////////////////////////////////// top story rotator
globalStory = 0;
theStorySpeed = 15000;
function rotateStories(){
//******************************************call xml
////////////// start load xml
var p;
var e;
try {
p = new XMLHttpRequest();
} 
catch (e) {
p = new ActiveXObject("Msxml2.XMLHTTP");
}
var interactiveCount = 0;
function myfunc()
{
if (p.readyState == 3) {interactiveCount++;}
if (p.readyState != 4)
return;
if (p.responseXML) {var str;
try { var s = new XMLSerializer();
      var d = p.responseXML;
      str = s.serializeToString(d);
    } 
catch (e) {str = p.responseXML.xml;}
///////// set arrays

var x2=p.responseXML.getElementsByTagName("story");
topStory = new Array();
for (i=0;i<3;i++)
{ 
topStory[i] = new Array();
topStory[i][0]=x2[i].getElementsByTagName("storyPic")[0].childNodes[0].nodeValue;
topStory[i][1]=x2[i].getElementsByTagName("storyCaption")[0].childNodes[0].nodeValue;
topStory[i][2]=x2[i].getElementsByTagName("storyHead")[0].childNodes[0].nodeValue;
topStory[i][3]=x2[i].getElementsByTagName("storyBlurb")[0].childNodes[0].nodeValue;
topStory[i][4]=x2[i].getElementsByTagName("storyLink")[0].childNodes[0].nodeValue;

}
//////////////////////////////////////////// write nav
displayStory();
playStory = setInterval(checkStory,theStorySpeed);
}
}
// p.onload would also work in Mozilla
p.onreadystatechange = myfunc;
try {
  netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
} catch (e) {  // ignore
}
p.open("GET", "xml/xml_topstory.xml");
p.send(null);
//******************************************end call xml
}


function displayStory(){
writePic = "<img src=\""+topStory[globalStory][0]+"\" />";
writeCap = topStory[globalStory][1];
writeHead = topStory[globalStory][2];
writeBlurb = topStory[globalStory][3];
writeBlurb += "<a href=\""+topStory[globalStory][4]+"\">(Full Story)</a>";
document.getElementById("topStoryPic").innerHTML=writePic;
document.getElementById("topStoryCap").innerHTML=writeCap;
document.getElementById("topStoryHead").innerHTML=writeHead;
document.getElementById("topStoryBlurb").innerHTML=writeBlurb;
document.getElementById("st_0").className = "";
document.getElementById("st_1").className = "";
document.getElementById("st_2").className = "";
stChoice = "st_"+globalStory;
document.getElementById(stChoice).className = "frontControlsHigh";
}
function initiateStory(){
displayStory();
playStory = setInterval(checkStory,theStorySpeed);
}
function checkStory(){
globalStory++;
if(globalStory>2){globalStory=0;}
displayStory();
}

function executeStory(mode,story){
clearInterval(playStory);
switch(mode){
case 1://pause
break;
case 2://play previous
globalStory--;
if(globalStory<0){globalStory=2}
displayStory();
playStory = setInterval(checkStory,theStorySpeed);
break;
case 3://play next
globalStory++;
if(globalStory>2){globalStory=0}
displayStory();
playStory = setInterval(checkStory,theStorySpeed);
break;
case 4://play specific
globalStory=story;
displayStory();
playStory = setInterval(checkStory,theStorySpeed);
break;
}
}
////////////////////////////////////// end top story rotator
////////////////////////////////////// set Nav
function setNav(){

//******************************************call xml
////////////// start load xml
var p;
var e;
try {
p = new XMLHttpRequest();
} 
catch (e) {
p = new ActiveXObject("Msxml2.XMLHTTP");
}
var interactiveCount = 0;
function myfunc()
{
if (p.readyState == 3) {interactiveCount++;}
if (p.readyState != 4)
return;
if (p.responseXML) {var str;
try { var s = new XMLSerializer();
      var d = p.responseXML;
      str = s.serializeToString(d);
    } 
catch (e) {str = p.responseXML.xml;}
///////// set arrays

var x2=p.responseXML.getElementsByTagName("nav");
topNav = new Array();
for (i=0;i<x2.length;i++)
{ 
topNav[i] = new Array();
topNav[i][0] = new Array();
topNav[i][0][0]=x2[i].getElementsByTagName("navName")[0].childNodes[0].nodeValue;
topNav[i][0][1]=x2[i].getElementsByTagName("navLink")[0].childNodes[0].nodeValue;
topNav[i][0][2]=x2[i].getElementsByTagName("navID")[0].childNodes[0].nodeValue;

var x21=x2[i].getElementsByTagName("navSubs");
topNav[i][1] = new Array();
for (a=0;a<x21.length;a++){
topNav[i][1][a] = new Array();
topNav[i][1][a][0]=x21[a].getElementsByTagName("navName")[0].childNodes[0].nodeValue;
topNav[i][1][a][1]=x21[a].getElementsByTagName("navLink")[0].childNodes[0].nodeValue;
topNav[i][1][a][2]=x21[a].getElementsByTagName("navID")[0].childNodes[0].nodeValue;
}
}
//////////////////////////////////////////// write nav
myNav = "";
/////////////////write main nav
mySub="";
for(i=0;i<topNav.length;i++){
myNav+="<a href=\""+topNav[i][0][1]+"\" onMouseOver=\"showSub("+topNav[i][0][2]+");\" onMouseOut=\"checkState();\" id=\"top_"+topNav[i][0][2]+"\">"+topNav[i][0][0]+"</a>"
if(i!=topNav.length-1){myNav+="<p></p>"}
/////////////////write sub nav
mySub += "<div id=\"subNav"+topNav[i][0][2]+"\" style=\"display:none;\">";
for(a=0;a<topNav[i][1].length;a++){
mySub+="<a href=\""+topNav[i][1][a][1]+"\" onMouseOver=\"clearOut();\" onMouseOut=\"checkState();\" id=\"sub_"+topNav[i][1][a][2]+"\">"+topNav[i][1][a][0]+"</a>"
if(a!=topNav[i][1].length-1){mySub+="<p></p>"}
}
mySub += "</div>";
}
document.getElementById("mainNav").innerHTML=myNav;
document.getElementById("sNav").innerHTML=mySub;
if((theMarker=="") || (theMarker==undefined)){}else{
setMainStyle(theMarker,theSubMarker);showSub(theMarker);
}

////////////////////////////////////////// end write nav
}
}
// p.onload would also work in Mozilla
p.onreadystatechange = myfunc;
try {
  netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
} catch (e) {  // ignore
}
p.open("GET", "xml/xml_nav.xml");
p.send(null);
//******************************************end call xml
}


/////////////////write  nav
mynavTime = "";

function showSub(tSub){
for(i=0;i<topNav.length;i++){
document.getElementById("top_"+topNav[i][0][2]).className = "offNav";
}
clearInterval(mynavTime);
hideSubs()
if((theSubMarker=="") || (theSubMarker==undefined)) {}else{
document.getElementById("subNav"+tSub).style.display = "inline";
}
document.getElementById("top_"+tSub).className = "hightLightNav";
}

function clearSub(){
if((theMarker=="") || (theMarker==undefined)){
hideSubs();
} else {
showSub(theMarker);
setMainStyle(theMarker,theSubMarker);
}
clearInterval(mynavTime);
}

function checkState(){
mynavTime = setInterval(clearSub,1000);
}

function setMainStyle(theID,theSubID){
document.getElementById("top_"+theID).className = "hightLightNav";

if((theSubMarker=="") || (theSubMarker==undefined)) {}else{
document.getElementById("sub_"+theSubID).className = "hightLightSub";
document.getElementById("subNav"+theID).style.display = "inline";}
}


function hideSubs(){
for(i=0;i<topNav.length;i++){
document.getElementById("subNav"+topNav[i][0][2]).style.display = "none";
document.getElementById("top_"+topNav[i][0][2]).className = "offNav";
}
}

function clearOut(){
clearInterval(mynavTime);
}

////////////////////////////////////// end Nav
////////////////////////////////////// top story rotator
//teamNum = 0;
function teamProfiles(){
//******************************************call xml
////////////// start load xml
var p;
var e;
try {
p = new XMLHttpRequest();
} 
catch (e) {
p = new ActiveXObject("Msxml2.XMLHTTP");
}
var interactiveCount = 0;
function myfunc()
{
if (p.readyState == 3) {interactiveCount++;}
if (p.readyState != 4)
return;
if (p.responseXML) {var str;
try { var s = new XMLSerializer();
      var d = p.responseXML;
      str = s.serializeToString(d);
    } 
catch (e) {str = p.responseXML.xml;}
///////// set arrays

var x2=p.responseXML.getElementsByTagName("player");
teamMembers = new Array();

for (i=0;i<x2.length;i++)
{ 

teamMembers[i] = new Array();
teamMembers[i][0]=x2[i].getElementsByTagName("pPic")[0].childNodes[0].nodeValue;
teamMembers[i][1]=x2[i].getElementsByTagName("pName")[0].childNodes[0].nodeValue;
teamMembers[i][2]=x2[i].getElementsByTagName("pPosition")[0].childNodes[0].nodeValue;
teamMembers[i][3]=x2[i].getElementsByTagName("pLink")[0].childNodes[0].nodeValue;
}
//////////////////////////////////////////// write nav
showTeam();
}
}
// p.onload would also work in Mozilla
p.onreadystatechange = myfunc;
try {
  netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
} catch (e) {  // ignore
}
p.open("GET", "xml/xml_team.xml");
p.send(null);
//******************************************end call xml
}


function showTeam(direction){
if(direction==1){
teamNum++;
if(teamNum>=teamMembers.length){teamNum=0}
}else if(direction==0) {
teamNum--;
if(teamNum<0){teamNum=teamMembers.length-1}
} else {
teamNum = Math.ceil(Math.random()*teamMembers.length)-1;
}
tPic = teamMembers[teamNum][0];
tName = teamMembers[teamNum][1];
tPos = teamMembers[teamNum][2];
tLink = teamMembers[teamNum][3];
document.getElementById("teamPic").innerHTML="<img src=\""+tPic+"\" width='65px' height='65px'>";
document.getElementById("teamName").innerHTML="<a href=\""+tLink+"\" id=\"teamName\">"+tName+" - <strong>"+tPos+"</strong></a>";

}

////////////////////////////////////// breaking news rotator
newsNum = 1;
function breakingRotator(){

//******************************************call xml
////////////// start load xml
var p;
var e;
try {
p = new XMLHttpRequest();
} 
catch (e) {
p = new ActiveXObject("Msxml2.XMLHTTP");
}
var interactiveCount = 0;
function myfunc()
{
if (p.readyState == 3) {interactiveCount++;}
if (p.readyState != 4)
return;
if (p.responseXML) {var str;
try { var s = new XMLSerializer();
      var d = p.responseXML;
      str = s.serializeToString(d);
    } 
catch (e) {str = p.responseXML.xml;}
///////// set arrays

var x2=p.responseXML.getElementsByTagName("breakers");
newsHeads = new Array();
for (i=0;i<x2.length;i++)
{ 
newsHeads[i] = new Array();
newsHeads[i][0]=x2[i].getElementsByTagName("bHead")[0].childNodes[0].nodeValue;
newsHeads[i][1]=x2[i].getElementsByTagName("bLink")[0].childNodes[0].nodeValue;
}
//////////////////////////////////////////// write nav
showNews();
newsTimer = setInterval(showNews,5000);
}
}
// p.onload would also work in Mozilla
p.onreadystatechange = myfunc;
try {
  netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
} catch (e) {  // ignore
}
p.open("GET", "xml/xml_breakingnews.xml");
p.send(null);
//******************************************end call xml
}


function showNews(){
newsNum++;
if(newsNum>=newsHeads.length){newsNum=0}
newsHead = newsHeads[newsNum][0];
newsLink = newsHeads[newsNum][1];
newsCopy = " <a href=\""+newsLink+"\">"+newsHead+" (Full Story)</a>";
document.getElementById("newsHeadlines").innerHTML=newsCopy;
}



