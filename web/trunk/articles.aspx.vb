Partial Class articles
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Articles(ID)
    End Sub
    Sub Articles(ByVal Id As String)
        SqlQuery = New SqlCommand("Select Top 1 a.Id, a.Headline, a.Body, a.Created, a.LargeImage, a.LargeImageAlt, c.Name As Author, d.Name As Credit From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneAuthors c ON c.Id = a.Author INNER JOIN SuperSportZone.dbo.ZoneCredits d ON d.Id = a.Credit Where (a.Id = " & Request.QueryString("Id") & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            Dim Headline As String = RsRec("Headline")
            Dim Body As String = RsRec("Body")
            Dim ArticleDate As DateTime = RsRec("Created")
            Dim Image As String = RsRec("LargeImage")
            Dim ImageAlt As String = RsRec("LargeImageAlt")
            Dim Credit As String = RsRec("Credit")
            Dim Author As String = RsRec("Author")
            ltlarticle.Text &= "<div style='background-color:#ffffff;width:492px;text-align:left;'>"
            ltlarticle.Text &= "<div class='content'>"

            ltlarticle.Text &= "<div style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 0px;'>"
            ltlarticle.Text &= "<table border='0' cellpadding='0' cellspacing='0' width='485px'><tr>"
            ltlarticle.Text &= "<td style='text-align:left;padding-left:10px;width:100%;font-weight:italics;padding-top:3px;'>Posted : " & ArticleDate.ToString("dddd', 'dd '&nbsp;' MM'&nbsp;'yyyy  hh':'mm") & "</td></tr></table>"
            ltlarticle.Text &= "</div>"
            ltlarticle.Text &= "<div class='contentbody'>"
            If Image <> "" Then
                ltlarticle.Text &= "<table cellspacing='0' cellpadding='0' border='0' align='left' ><tr><td style='padding-left: 2px;'><img src='http://images.supersport.com/" & Image & "' alt='" & ImageAlt & "' border='0' style='padding-Top:3px;padding-bottom:3px;padding-right:3px;' /></td></tr>"
                If ImageAlt <> "" Then
                    ltlarticle.Text &= "<tr><td align='center'><div style='width: 55%; text-align: center; font-style: italic; padding-bottom: 5px;'>" & ImageAlt & "</div></td></tr>"
                End If
                ltlarticle.Text &= "</table>"
            End If
            Const pattern As String = "(\[embed:)(.+)(\])"
            Body = Regex.Replace(Body, pattern, String.Empty, RegexOptions.IgnoreCase)
            ltlarticle.Text &= "<b>" & UCase(Headline) & "</b><br><br>" & Body & "</div>"
            If Author <> "" Then
                Author = "By " & Author
            End If
            If Credit <> "" Then
                Credit = "&copy; " & Credit
            End If
            If Credit <> "" Or Author <> "" Then
                ltlarticle.Text &= "<div style='padding-top: 7px;'><div style='text-align: right; float: left; font-style: italic;width:100%;'>" & Author & "&nbsp;" & Credit & "</div></div>"
            End If
            ltlarticle.Text &= "</div>"
            ltlarticle.Text &= "</div>"

        End While
        RsRec.Close()
    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class
