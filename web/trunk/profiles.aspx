<%@ Page Language="VB" AutoEventWireup="false" CodeFile="profiles.aspx.vb" Inherits="profiles" MasterPageFile="master.master" %>
<%@ MasterType VirtualPath="master.master" %>

<asp:Content runat="server" ContentPlaceHolderId="ContentPlaceHolder1" Id="content1">

    <asp:Panel id="pnl1" runat="server" EnableViewState="false">
        <table cellspacing="0" cellpadding="0" border="0" width="492px" style="background-color:#ffffff;">
        <tr>
        <td  style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>PROFILES</td>
        </tr>
            <tr>
                <td valign="top"><div class="content">
                    <table width="492px" cellspacing="1" cellpadding="0" border="0">
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#1e2b5f">
                                    <tr>
        <td  style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>Goalkeepers</td>
        </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltlgoalkeepers" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="7"></td>
                        </tr>
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#1e2b5f">
                                                <tr>
        <td  style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>Defenders</td>
        </tr>

                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltldefenders" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="7"></td>
                        </tr>
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#1e2b5f">
          <tr>
        <td  style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>Midfielders</td>
        </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltlmidfielders" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="7"></td>
                        </tr>
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellspacing="1" cellpadding="0" border="0" bgcolor="#1e2b5f">
                                          <tr>
        <td  style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>Forwards</td>
        </tr>

                                    <tr>
                                        <td bgcolor="#FFFFFF" style="padding: 2px; padding-top: 5px; padding-bottom: 5px;">
                                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                <asp:Literal id="ltlforwards" runat="server" EnableViewState="false" />
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div></td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel id="pnl2" runat="server" EnableViewState="false">
        <table cellspacing="0" cellpadding="0" border="0"  style="background-color:#ffffff;text-align:left;width:492px;">
            <tr>
        <td  style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>PROFILE : <asp:Literal id="ltlname" runat="server" EnableViewState="false" /></td>
        </tr>
        <tr>    
                <td class="content"><div class="content">
                    <asp:Literal id="ltlprofile" runat="server" EnableViewState="false" />
                </div></td>
            </tr>
              </table>
    </asp:Panel>
    
</asp:Content>