
Partial Class rss_feed
    Inherits System.Web.UI.Page

    Private SiteId As Integer = ConfigurationManager.AppSettings("Site")
    Private Sport As String = ConfigurationManager.AppSettings("SiteSport")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8

        DatabaseConn.Open()

        Dim ws As New XmlWriterSettings()
        ws.CheckCharacters = True
        ws.CloseOutput = False
        ws.Indent = True
        Dim utf8 As Encoding
        utf8 = Encoding.UTF8
        ws.Encoding = utf8

        Dim CategoryId As Integer = 0
        Dim SiteId As Integer = 0
        Dim Heading As String = "Latest News"
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        If Not Request.QueryString("site") Is Nothing Then
            SiteId = Request.QueryString("site")
        End If

        If Not Request.QueryString("cat") Is Nothing Then
            CategoryId = Request.QueryString("cat")
        End If


        If CategoryId > 0 Then
            SqlQuery = New SqlCommand("Select Displayname From SuperSportZone.dbo.ZoneCategories Where (Id = " & CategoryId & ")", DatabaseConn)
            Heading = SqlQuery.ExecuteScalar
            SqlQuery = New SqlCommand("Select Top 30 a.Id, a.Modified ,a.Headline, a.Blurb, a.SmallImage , a.SmallImageAlt,b.ArticleDate,b.ArticleLevel From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
            da.SelectCommand = SqlQuery
            da.Fill(ds, "news")
        ElseIf SiteId > 0 Then
            SqlQuery = New SqlCommand("Select Display_name From SuperSportZone.dbo.ZoneSites Where (Id = " & SiteId & ")", DatabaseConn)
            Heading = SqlQuery.ExecuteScalar
            SqlQuery = New SqlCommand("Select Top 30 a.Id, a.Modified ,a.Headline, a.Blurb, a.SmallImage , a.SmallImageAlt,b.ArticleDate,b.ArticleLevel From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
            da.SelectCommand = SqlQuery
            da.Fill(ds, "news")
        Else
            SqlQuery = New SqlCommand("Select Top 30 a.Id, a.Modified ,a.Headline, a.Blurb, a.SmallImage , a.SmallImageAlt,b.ArticleDate,b.ArticleLevel From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
            da.SelectCommand = SqlQuery
            da.Fill(ds, "news")
        End If

        Dim tmpRows() As DataRow = ds.Tables("news").Select("", "Modified Desc")
        Dim Modified As DateTime = tmpRows(0).Item("Modified")

        Using tmpWriter As XmlWriter = XmlWriter.Create(Response.Output, ws)
            tmpWriter.WriteStartDocument()
            tmpWriter.WriteStartElement("rss")
            tmpWriter.WriteAttributeString("version", "2.0")
            tmpWriter.WriteStartElement("channel")

            tmpWriter.WriteElementString("title", "SuperSport United Football Club News | " & Heading)
            tmpWriter.WriteElementString("link", "http://www.sufc.co.za")
            tmpWriter.WriteElementString("description", "Get all the latest Sport news and statistics.")
            tmpWriter.WriteElementString("language", "en-gb")
            tmpWriter.WriteElementString("lastBuildDate", Modified.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"))
            tmpWriter.WriteElementString("copyright", "Copyright: (C) SuperSport Zone")
            tmpWriter.WriteElementString("ttl", "15")

            tmpWriter.WriteStartElement("image")
            tmpWriter.WriteElementString("title", "SuperSport United Football Club News")
            tmpWriter.WriteElementString("url", "http://images.supersport.com/ss_rss_120x43.jpg")
            tmpWriter.WriteElementString("link", "http://www.sufc.co.za")
            tmpWriter.WriteEndElement()

            tmpRows = ds.Tables("news").Select("", "ArticleLevel Desc, ArticleDate Desc, Id Desc")
            Dim N As Integer
            For N = 0 To tmpRows.Length - 1
                Dim tmpDate As DateTime = tmpRows(N).Item("Modified")
                tmpWriter.WriteStartElement("item")
                tmpWriter.WriteElementString("title", tmpRows(N).Item("Headline"))
                tmpWriter.WriteElementString("description", RemoveHtml(tmpRows(N).Item("Body")))
                If tmpRows(N).Item("Folder") <> "" Then
                    tmpWriter.WriteElementString("link", "http://www.sufc.co.za/" & tmpRows(N).Item("Folder") & "/articles.aspx?Id=" & tmpRows(N).Item("Id"))
                Else
                    tmpWriter.WriteElementString("link", "http://www.sufc.co.za/articles.aspx?Id=" & tmpRows(N).Item("Id"))
                End If
                tmpWriter.WriteElementString("author", tmpRows(N).Item("Author"))
                tmpWriter.WriteElementString("pubDate", tmpDate.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"))
                tmpWriter.WriteElementString("category", tmpRows(N).Item("Category"))
                If tmpRows(N).Item("SmallImage") <> "" Then
                    tmpWriter.WriteStartElement("enclosure")
                    tmpWriter.WriteAttributeString("url", "http://images.supersport.com/" & tmpRows(N).Item("SmallImage"))
                    tmpWriter.WriteAttributeString("length", "1")
                    tmpWriter.WriteAttributeString("type", "image/jpeg")
                    tmpWriter.WriteEndElement()
                End If

                tmpWriter.WriteEndElement()
            Next

            tmpWriter.WriteEndElement()
            tmpWriter.WriteEndElement()
            tmpWriter.WriteEndDocument()
        End Using

    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload

        DatabaseConn.Close()
        DatabaseConn.Dispose()

    End Sub

    Private Function RemoveHtml(ByVal strText As String)

        Dim retText As String
        Dim Repeat As Boolean
        Dim Position1 As Integer = 0
        Dim Position2 As Integer = 0
        Dim Position3 As Integer = 0
        Dim TheText As String

        retText = "|||" & strText

        retText = retText.Replace("" & Environment.NewLine & "", " ")

        Repeat = True
        While Repeat = True
            If Position3 <= retText.Length Then
                Position1 = retText.IndexOf("<", Position3)
                If Position1 >= 0 Then
                    Position2 = retText.IndexOf(">", Position1 + 1)
                    If Position1 >= 0 And Position2 >= 0 Then
                        Position3 = Position2 + 1
                        TheText = retText.Substring(Position1, Position2 - Position1)
                        retText = retText.Remove(Position1, Position2 - Position1)
                        retText = retText.Insert(Position1, TheText.ToLower)
                    Else
                        Repeat = False
                    End If
                Else
                    Repeat = False
                End If
            Else
                Repeat = False
            End If
        End While

        retText = retText.Replace("</a><p>", "</a>")
        retText = retText.Replace("</a> <p>", "</a>")
        retText = retText.Replace("</a><br>", "</a>")
        retText = retText.Replace("</a> <br>", "</a>")
        retText = retText.Replace("&nbsp;", " ")
        retText = retText.Replace("&quot;", "''")
        retText = retText.Replace("&amp;", "and")

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<a")
            Position2 = retText.IndexOf("</a>", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 4
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("&")
            Position2 = retText.IndexOf(";", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 1
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<table")
            Position2 = retText.IndexOf("</table>", Position1 + 1)
            If Position1 >= 0 And Position2 > 0 Then
                Position2 = Position2 + 8
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<pre")
            Position2 = retText.IndexOf("</pre>", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 6
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<spa")
            Position2 = retText.IndexOf("</span>", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 7
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<div")
            Position2 = retText.IndexOf("</div>", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 6
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<sup")
            Position2 = retText.IndexOf("</sup>", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 6
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        retText = retText.Replace("<p  ", "<p>")
        retText = retText.Replace("<br  ", "<br>")
        retText = retText.Replace("<p.", "<p>")
        retText = retText.Replace("<br.", "<br>")

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<p ")
            Position2 = retText.IndexOf(">", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 1
                retText = retText.Remove(Position1, Position2 - Position1)
                retText = retText.Insert(Position1, "<p>")
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<br ")
            Position2 = retText.IndexOf(">", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 1
                retText = retText.Remove(Position1, Position2 - Position1)
                retText = retText.Insert(Position1, "<br>")
            Else
                Repeat = False
            End If
        End While

        retText = retText.Replace("<p>", "<br/><br/>")
        retText = retText.Replace("<br>", "<br/>")
        retText = retText.Replace("</p>", "")
        retText = retText.Replace("</br>", "<br/>")
        retText = retText.Replace("<br/>", Environment.NewLine)

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("<")
            Position2 = retText.IndexOf(">", Position1 + 1)
            If Position1 > 0 And Position2 > 0 Then
                Position2 = Position2 + 1
                retText = retText.Remove(Position1, Position2 - Position1)
            Else
                Repeat = False
            End If
        End While

        Repeat = True
        While Repeat = True
            Position1 = retText.IndexOf("  ")
            Position2 = Position1 + 2
            If Position1 >= 0 Then
                retText = retText.Remove(Position1, Position2 - Position1)
                retText = retText.Insert(Position1, " ")
            Else
                Repeat = False
            End If
        End While

        retText = retText.Replace(Environment.NewLine & " ", Environment.NewLine)
        retText = retText.Replace(" & ", " and ")
        retText = retText.Replace("& ", "and ")
        retText = retText.Replace(" &", " and")
        retText = retText.Replace("&", "+")

        Dim utf8 As System.Text.UTF8Encoding = New System.Text.UTF8Encoding
        Dim encBytes As Byte() = utf8.GetBytes(retText)
        Dim Decoded As Char() = utf8.GetChars(encBytes)
        Dim varChar As Char
        Dim varByte As Byte
        Dim ByteCount As Integer
        For Each varChar In Decoded
            ByteCount = utf8.GetByteCount(varChar)
            If ByteCount > 2 Then
                If ByteCount = 3 Then
                    Dim ByteArray() As Byte
                    ByteArray = utf8.GetBytes(varChar)
                    If ByteArray(0) = 226 And ByteArray(1) = 128 And ByteArray(2) = 153 Then
                        retText = retText.Replace(varChar, "'")
                    ElseIf ByteArray(0) = 226 And ByteArray(1) = 128 And ByteArray(2) = 156 Then
                        retText = retText.Replace(varChar, "''")
                    ElseIf ByteArray(0) = 226 And ByteArray(1) = 128 And ByteArray(2) = 157 Then
                        retText = retText.Replace(varChar, "''")
                    Else
                        'lstbChanges("add", varChar)
                        retText = retText.Replace(varChar, "")
                    End If
                Else
                    retText = retText.Replace(varChar, "")
                End If
            End If
        Next

        retText = retText.Replace("|||", "")
        Dim Present As Boolean = True
        While Present = True
            If retText.StartsWith(Environment.NewLine) Then
                retText = retText.Substring(2)
            Else
                Present = False
            End If
        End While

        retText = retText.Replace(" " & Environment.NewLine, Environment.NewLine)
        retText = retText.Replace(Environment.NewLine & Environment.NewLine & Environment.NewLine & Environment.NewLine, Environment.NewLine & Environment.NewLine)
        retText = retText.Replace(Environment.NewLine & Environment.NewLine & Environment.NewLine, Environment.NewLine & Environment.NewLine)

        Return retText

    End Function

End Class
