
Partial Class rss_default
    Inherits System.Web.UI.Page

    Private SiteId As Integer = ConfigurationManager.AppSettings("Site")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        Response.ContentType = "text/xml"
        Response.ContentEncoding = Encoding.UTF8

        DatabaseConn.Open()

        Dim ws As New XmlWriterSettings()
        ws.CheckCharacters = True
        ws.CloseOutput = False
        ws.Indent = True
        Dim utf8 As Encoding
        utf8 = Encoding.UTF8
        ws.Encoding = utf8

        Dim CategoryId As Integer = 0
        Dim SiteId As Integer = 0
        Dim Heading As String = "Latest News"
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter

        If Not Request.QueryString("site") Is Nothing Then
            SiteId = Request.QueryString("site")
        End If

        If Not Request.QueryString("cat") Is Nothing Then
            CategoryId = Request.QueryString("cat")
        End If

        If CategoryId > 0 Then
            SqlQuery = New SqlCommand("Select Displayname From SuperSportZone.dbo.ZoneCategories Where (Id = " & CategoryId & ")", DatabaseConn)
            Heading = SqlQuery.ExecuteScalar
            SqlQuery = New SqlCommand("Select Top 30 a.Id, a.Modified ,a.Headline, a.Blurb, a.SmallImage , a.SmallImageAlt,b.ArticleDate,b.ArticleLevel From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
            da.SelectCommand = SqlQuery
            da.Fill(ds, "news")
        ElseIf SiteId > 0 Then
            SqlQuery = New SqlCommand("Select Display_name From SuperSportZone.dbo.ZoneSites Where (Id = " & SiteId & ")", DatabaseConn)
            Heading = SqlQuery.ExecuteScalar
            SqlQuery = New SqlCommand("Select Top 30 a.Id, a.Modified ,a.Headline, a.Blurb, a.SmallImage , a.SmallImageAlt,b.ArticleDate,b.ArticleLevel From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
            da.SelectCommand = SqlQuery
            da.Fill(ds, "news")
        Else
            SqlQuery = New SqlCommand("Select Top 30 a.Id, a.Modified ,a.Headline, a.Blurb, a.SmallImage, a.SmallImageAlt,b.ArticleDate,b.ArticleLevel From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
            da.SelectCommand = SqlQuery
            da.Fill(ds, "news")
        End If

        Dim tmpRows() As DataRow = ds.Tables("news").Select("", "Modified Desc")
        Dim Modified As DateTime = tmpRows(0).Item("Modified")

        Using tmpWriter As XmlWriter = XmlWriter.Create(Response.Output, ws)
            tmpWriter.WriteStartDocument()
            tmpWriter.WriteStartElement("rss")
            tmpWriter.WriteAttributeString("version", "2.0")
            tmpWriter.WriteStartElement("channel")

            tmpWriter.WriteElementString("title", "SuperSport United Football Club News | " & Heading)
            tmpWriter.WriteElementString("link", "http://www.sufc.co.za")
            tmpWriter.WriteElementString("description", "Get all the latest Sport news and statistics.")
            tmpWriter.WriteElementString("language", "en-gb")
            tmpWriter.WriteElementString("lastBuildDate", Modified.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"))
            tmpWriter.WriteElementString("copyright", "Copyright: (C) SuperSport Zone")
            tmpWriter.WriteElementString("ttl", "15")

            tmpWriter.WriteStartElement("image")
            tmpWriter.WriteElementString("title", "SuperSport United Football Club News")
            tmpWriter.WriteElementString("url", "http://images.supersport.com/ss_rss_120x43.jpg")
            tmpWriter.WriteElementString("link", "http://www.sufc.co.za")
            tmpWriter.WriteEndElement()

            tmpRows = ds.Tables("news").Select("", "ArticleLevel Desc, ArticleDate Desc, Id Desc")
            Dim N As Integer
            For N = 0 To tmpRows.Length - 1
                Dim tmpDate As DateTime = tmpRows(N).Item("Modified")
                tmpWriter.WriteStartElement("item")
                tmpWriter.WriteElementString("title", tmpRows(N).Item("Headline"))
                tmpWriter.WriteElementString("description", tmpRows(N).Item("Blurb"))
                '   If tmpRows(N).Item("Folder") <> "" Then
                'tmpWriter.WriteElementString("link", "http://www.supersport.com/" & tmpRows(N).Item("Folder") & "/article.aspx?Id=" & tmpRows(N).Item("Id"))
                '  Else
                ' tmpWriter.WriteElementString("link", "http://www.supersport.com/article.aspx?Id=" & tmpRows(N).Item("Id"))
                ' End If
                'tmpWriter.WriteElementString("author", tmpRows(N).Item("Author"))
                tmpWriter.WriteElementString("pubDate", tmpDate.AddHours(-2).ToString("ddd',' dd MMM yyyy HH':'mm':'ss 'GMT'"))
                'tmpWriter.WriteElementString("category", tmpRows(N).Item("Category"))
                If tmpRows(N).Item("SmallImage") <> "" Then
                    tmpWriter.WriteStartElement("enclosure")
                    tmpWriter.WriteAttributeString("url", "http://images.supersport.com/" & tmpRows(N).Item("SmallImage"))
                    tmpWriter.WriteAttributeString("length", "1")
                    tmpWriter.WriteAttributeString("type", "image/jpeg")
                    tmpWriter.WriteEndElement()
                End If

                tmpWriter.WriteEndElement()
            Next

            tmpWriter.WriteEndElement()
            tmpWriter.WriteEndElement()
            tmpWriter.WriteEndDocument()
        End Using

    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload

        DatabaseConn.Close()
        DatabaseConn.Dispose()

    End Sub

End Class
