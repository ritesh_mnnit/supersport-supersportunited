﻿ver = navigator.appVersion
agent = navigator.userAgent.toLowerCase()
dom = document.getElementById ? 1 : 0
ns4 = (!dom && document.layers) ? 1 : 0;
op = window.opera
moz = (agent.indexOf("gecko") > -1 || window.sidebar)
ie = agent.indexOf("msie") > -1 && !op
var req

function archive(id) {
    for (var i = 1; i<= 5; i++) {
        if (parseInt(id) == i) {
            document.getElementById("archive" + i).className = "archive_selected";
            document.getElementById("archives" + i).style.display = "block";
        } else {
            document.getElementById("archive" + i).className = "archive";
            document.getElementById("archives" + i).style.display = "none";
        }
    }
}

var div;

function change(divId, catId, pageId) {
    var url;
    var d = new Date();
    var time = d.getHours() + '' + d.getMinutes() + '' + d.getSeconds()
    url = "/videoarchive.aspx?cat="+catId+"&page="+pageId+"&div="+divId+"&time="+time
    div = divId;
    if ((ns4) || (op) || (moz)) {
        element = document.getElementById("archives" + divId)
        req = new XMLHttpRequest();
    } else if (ie) {
        element = document.getElementById("archives" + divId)
        req = new ActiveXObject("Microsoft.XMLHTTP");
    }
    element.innerHTML = ""
    req.onreadystatechange = processReqChange;
    req.open("GET", url, true);
    if ((ns4) || (op) || (moz)) {
        req.send(null);
    } else {
        req.send();
    }
}

function processReqChange() {
    if (req.readyState == 4) {
        var results = req.responseText;
        if (req.status == 200) {
            document.getElementById("archives" + div).innerHTML = results;
        } else {
            document.getElementById("archives" + div).innerHTML = "Error retrieving video list";
        }
    }
}
