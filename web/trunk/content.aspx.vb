
Partial Class content
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer

    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Content(ID)
    End Sub
    Sub Content(ByVal Id As String)
        SqlQuery = New SqlCommand("SELECT Id As Content_Id, Headline As Content_Headline, Content As Content_Body FROM ZoneContent WHERE (Id = " & Request.QueryString("Id") & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlcontent.Text &= "<div style='background-color:#ffffff;width:492px;text-align:left;'>"
            ltlcontent.Text &= "<div class='content'>"
            ltlcontent.Text &= "<div style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 0px;'>"
            ltlcontent.Text &= "<table border='0' cellpadding='0' cellspacing='0' width='485px' ><tr><td style='width:50%;padding-left:3px;padding-top:3px;'>" & RsRec("Content_Headline") & "</td>"
            ltlcontent.Text &= "</tr></table>"
            ltlcontent.Text &= "</div>"
            ltlcontent.Text &= "<div class='contentbody'>"
            ltlcontent.Text &= "<div style='background-color:#ffffff;width:492px;text-align:left;'>"
            ltlcontent.Text &= "<table cellspacing='0' cellpadding='0' border='0' align='left' >"
            ltlcontent.Text &= "<tr><td align='center' style='background-color:#ffffff;width:492px;text-align:left;'><div style='width: 100%; text-align: center; font-style: normal; padding-bottom: 5px;text-align:left;'>" & RsRec("Content_Body") & "</div></td></tr>"
            ltlcontent.Text &= "</table></div>"
            ltlcontent.Text &= "</div>"
            ltlcontent.Text &= "</div>"
        End While
        RsRec.Close()

    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub


End Class
