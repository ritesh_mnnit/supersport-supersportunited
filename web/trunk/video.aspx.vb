
Partial Class video
    Inherits System.Web.UI.Page

    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("Site")
    Public TableWidth As Integer = System.Configuration.ConfigurationManager.AppSettings("SmallTableWidth")
    Public TopImage As String = "SUFC_adTop"
    Public BottomImage As String = "SUFC_adBottom"
    Private RightNav As Boolean = False
    Private Print As Boolean = False
    Public FileName As String = ""
    Public Background As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        DatabaseConn.Open()

        Dim CountryCode As String = ""
        Dim Show As String = ""

        Dim tmpId As String = ""
        Dim Id As String = ""
        Dim PageId As Integer = 1

        If Not Request.QueryString("id") Is Nothing Then
            tmpId = Request.QueryString("id")
            Id = Request.QueryString("id")
        End If

        If Not Request.QueryString("page") Is Nothing Then
            PageId = Request.QueryString("page")
        End If

        If Not Request.QueryString("show") Is Nothing Then
            Show = Request.QueryString("show").ToLower
        End If

        If Id = "" Then
            SqlQuery = New SqlCommand("Select Top 1 a.Id From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.SuperSportUnited = 1) And (a.Active = 1) Order By a.Created Desc, a.Id Desc", DatabaseConn)
            Id = SqlQuery.ExecuteScalar
        End If

        SqlQuery = New SqlCommand("Select Top 1 Category From SSZGeneral.dbo.BroadBandVideo Where (Id = " & Id & ")", DatabaseConn)
        Dim Category As Integer = SqlQuery.ExecuteScalar

        Dim Categories As Hashtable = New Hashtable
        Categories.Add(100, "Matches")
        Categories.Add(101, "1st Team")
        Categories.Add(102, "Academy")
        Categories.Add(103, "CSI")
        Categories.Add(104, "General")

        Dim CategoryList As SortedList = New SortedList
        CategoryList.Add(1, 100)
        CategoryList.Add(2, 101)
        CategoryList.Add(3, 102)
        CategoryList.Add(4, 103)
        CategoryList.Add(5, 104)

        If Categories.ContainsKey(Category) Then

        Else
            Category = 100
        End If

        SqlQuery = New SqlCommand("Select * From SSZGeneral.dbo.BroadBandVideo Where (Id = " & Id & ")", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            Dim Display As Boolean = True
            If Display = True Then
                ltl_headline.Text = RsRec("Name")
                Dim ClipLength As String = ""
                If RsRec("Length") <> "" And IsNumeric(RsRec("Length")) Then
                    Dim tmpLength As Integer = RsRec("Length")
                    If tmpLength <= 60 Then
                        ClipLength = "0:" & tmpLength.ToString
                    Else
                        If tmpLength Mod 60 > 0 Then
                            If tmpLength Mod 60 < 10 Then
                                ClipLength = (tmpLength - (tmpLength Mod 60)) / 60.ToString & ":0" & tmpLength Mod 60
                            Else
                                ClipLength = (tmpLength - (tmpLength Mod 60)) / 60.ToString & ":" & tmpLength Mod 60
                            End If
                        Else
                            ClipLength = (tmpLength - (tmpLength Mod 60)) / 60.ToString & ":00"
                        End If
                    End If
                End If
                ltl_length.Text = ClipLength
                ltl_headline.Text = RsRec("Name")
                ltl_detail.Text = RsRec("Description")
                FileName = RsRec("Video").Replace(".wmv", "")
                Background = RsRec("Preview")
            End If
        End While
        RsRec.Close()

        Dim Items As Integer = 6
        Dim IEnum As IEnumerator = CategoryList.GetEnumerator
        While IEnum.MoveNext
            Dim catId As Integer = IEnum.Current.Value
            Dim catNum As Integer = IEnum.Current.Key
            Dim catName As String = Categories(catId)

            If catId = Category Then
                archivemenu.Text &= "<div id=""archive" & catNum & """ class=""archive_selected"" onclick=""archive(" & catNum & ");""><div style=""position: relative; top: 8px;""><a href=""javascript:void(0);"">" & catName & "</a></div></div>"
                ltl_archive.Text &= "<div id=""archives" & catNum & """ style=""left: 0px; top: 0px; position: absolute; display: block; width: 100%;"">"
            Else
                archivemenu.Text &= "<div id=""archive" & catNum & """ class=""archive"" onclick=""archive(" & catNum & ");""><div style=""position: relative; top: 8px;""><a href=""javascript:void(0);"">" & catName & "</a></div></div>"
                ltl_archive.Text &= "<div id=""archives" & catNum & """ style=""left: 0px; top: 0px; position: absolute; display: none; width: 100%;"">"
            End If

            ltl_archive.Text &= "<table cellspacing=""4"" cellpadding=""7"" align=""Center"" border=""0"" style=""background-color: #ffffff; width:100%;"">"

            If catId = 100 Then
                SqlQuery = New SqlCommand("Select Count(a.Id) From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) And (Team1Name = 'SuperSport United' Or Team2Name = 'SuperSport United')", DatabaseConn)
            Else
                SqlQuery = New SqlCommand("Select Count(a.Id) From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) And (a.Category = " & catId & ")", DatabaseConn)
            End If
            Dim Total As Integer = Math.Min(SqlQuery.ExecuteScalar, 100)

            If catId = 100 Then
                SqlQuery = New SqlCommand("Select Top 6 a.Id, (Case When a.ShortName <> '' Then ShortName Else a.Name End) As Name, a.Created, (Case When a.ThumbNailImage = '' Then 'ss_logo_thumbnail.jpg' Else a.ThumbNailImage End) As Image, Description From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) And (Team1Name = 'SuperSport United' Or Team2Name = 'SuperSport United') Order By a.Created Desc, a.Id Desc", DatabaseConn)
            Else
                SqlQuery = New SqlCommand("Select Top 6 a.Id, (Case When a.ShortName <> '' Then ShortName Else a.Name End) As Name, a.Created, (Case When a.ThumbNailImage = '' Then 'ss_logo_thumbnail.jpg' Else a.ThumbNailImage End) As Image, Description From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) And (a.Category = " & catId & ") Order By a.Created Desc, a.Id Desc", DatabaseConn)
            End If
            RsRec = SqlQuery.ExecuteReader
            Dim Counter As Integer = 0
            While RsRec.Read
                If Counter = 0 Then
                    ltl_archive.Text &= "<tr>"
                Else
                    If Counter Mod 2 = 0 Then
                        ltl_archive.Text &= "</tr><tr>"
                    End If
                End If
                ltl_archive.Text &= "<td valign=""top"" style=""border: 1px solid #ffffff;color:#ffffff; background-color:#000032;width:50%;""><img src='http://cdn.dstv.com/supersport.img/videoimages/" & RsRec("Image") & "' width=""80"" height=""60"" align=""left"" style=""border-right: 5px solid #000032; "" /><a  href=""video.aspx?id=" & RsRec("Id") & """ style=""color:#ffffff;""><span style=""font-weight:bold;"">" & RsRec("Name") & "</span></a><br />" & RsRec("Description") & "</td>"
                Counter = Counter + 1
            End While
            RsRec.Close()
            If Counter > 0 Then
                ltl_archive.Text &= "</tr>"
            End If
            ltl_archive.Text &= "</table>"

            'ltl_archive.Text &= "<br />"
            ltl_archive.Text &= "<table width=""100%"" border=""0"" cellpadding=""0"" cellspacing=""0"">"
            ltl_archive.Text &= "<tr>"
            ltl_archive.Text &= "<td height=""26"" class=""article_btm_nav"">"
            ltl_archive.Text &= "<table width=""97%"" cellspacing=""0"" cellpadding=""0"" border=""0"" align=""center"">"
            ltl_archive.Text &= "<tr>"
            ltl_archive.Text &= "<td width=""33%"" align=""left""></td>"
            ltl_archive.Text &= "<td style=""color:#000000"" width=""33%"" align=""center"">"
            If Total > Items Then
                Dim Pages As Integer = 1
                If Total Mod Items > 0 Then
                    Pages = (Total - Total Mod Items) / Items
                    Pages = Pages + 1
                Else
                    Pages = Total / Items
                End If
                Dim I As Integer
                For I = 1 To Pages
                    ltl_archive.Text &= "<a style='color:#000000;font-weight:bold;padding-right: 15px;' href=""javascript:void(0);"" onclick=""change(" & catNum & "," & catId & "," & I & ");"">" & I & "</a>"
                Next
            End If
            ltl_archive.Text &= "</td>"
            ltl_archive.Text &= "<td width=""33%"" align=""right"" style='color:#000000;'>"
            If Total > Items Then
                ltl_archive.Text &= "<a style='color:#000000;font-weight:bold;' href=""javascript:void(0);"" onclick=""change(" & catNum & "," & catId & ",2);"">Next</a>"
            End If
            ltl_archive.Text &= "</td>"
            ltl_archive.Text &= "</tr>"
            ltl_archive.Text &= "</table>"
            ltl_archive.Text &= "</td>"
            ltl_archive.Text &= "</tr>"
            ltl_archive.Text &= "</table>"

            ltl_archive.Text &= "</div>"
        End While

    End Sub

End Class
