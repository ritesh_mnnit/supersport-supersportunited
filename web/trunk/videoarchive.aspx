﻿<%@ Page Language="VB" %>
<script runat="server">
    
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    
    Sub Page_Load()
        
        DatabaseConn.Open()
        
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        
        Dim Category As Integer = 102
        Dim PageId As Integer = 1
        Dim DivId As Integer = 1
        If Not Request.QueryString("cat") Is Nothing Then
            Category = Request.QueryString("cat")
        End If
        If Not Request.QueryString("page") Is Nothing Then
            PageId = Request.QueryString("page")
        End If
        If Not Request.QueryString("div") Is Nothing Then
            DivId = Request.QueryString("div")
        End If
        
        Dim SqlQuery As SqlCommand
        If Category = 100 Then
            SqlQuery = New SqlCommand("Select TOP 100 a.Id, (Case When a.ShortName <> '' Then ShortName Else a.Name End) As Name, a.Created, (Case When a.ThumbNailImage = '' Then 'ss_logo_thumbnail.jpg' Else a.ThumbNailImage End) As Image, Description From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) And (Team1Name = 'SuperSport United' Or Team2Name = 'SuperSport United') Order By a.Created Desc, a.Id Desc", DatabaseConn)
        Else
            SqlQuery = New SqlCommand("Select TOP 100 a.Id, (Case When a.ShortName <> '' Then ShortName Else a.Name End) As Name, a.Created, (Case When a.ThumbNailImage = '' Then 'ss_logo_thumbnail.jpg' Else a.ThumbNailImage End) As Image, Description From SSZGeneral.dbo.BroadBandVideo a INNER JOIN SSZGeneral.dbo.BroadBandVideoCategories b ON b.Id = a.Category Where (a.Active = 1) And (a.SuperSportUnited = 1) And (a.Category = " & Category & ") Order By a.Created Desc, a.Id Desc", DatabaseConn)
        End If

        Dim da As SqlDataAdapter = New SqlDataAdapter
        Dim ds As DataSet = New DataSet
        Dim tmpSource As PagedDataSource = New PagedDataSource
        da.SelectCommand = SqlQuery
        da.Fill(ds, "videos")
        Dim Total As Integer = ds.Tables("videos").Rows.Count
        Dim dv As DataView = ds.Tables("videos").DefaultView
        tmpSource.DataSource = dv
        tmpSource.AllowPaging = True
        tmpSource.PageSize = 6
        tmpSource.CurrentPageIndex = PageId - 1
        dlVideos.DataSource = tmpSource
        dlVideos.DataBind()
        da.Dispose()
        ds.Dispose()

        If tmpSource.IsFirstPage = False Then
            ltl_prev.Text = "<a style=""color:#000000;font-weight:bold;"" href=""javascript:void(0);"" onclick=""change(" & DivId & "," & Category & "," & PageId - 1 & ");"">Previous</a>"
        End If

        If tmpSource.PageCount > 1 Then
            Dim I As Integer
            For I = 1 To tmpSource.PageCount
                ltl_pages.Text &= "<a style='padding-right: 15px;color:#000000;font-weight:bold;' href=""javascript:void(0);"" onclick=""change(" & DivId & "," & Category & "," & I & ");"">" & I & "</a>"
            Next
        End If

        If tmpSource.IsLastPage = False And tmpSource.PageCount > 1 Then
            ltl_next.Text = "<a href=""javascript:void(0);"" style=""color:#000000;font-weight:bold;"" onclick=""change(" & DivId & "," & Category & "," & PageId + 1 & ");"">Next</a>"
        End If
        
        DatabaseConn.Close()

    End Sub

</script>

<%--
<table cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="background-color: #ffffff;color:#ffffff">
    <tr>
        <td align="center">
--%>            
            <asp:DataList id="dlVideos" RepeatColumns="2" RepeatDirection="Horizontal" runat="Server" Visible="True" Width="100%" CellSpacing="4" Height="100%" CellPadding="7" ItemStyle-VerticalAlign="top" ItemStyle-Width="50%" BackColor="#FFFFFF" ItemStyle-BackColor="#000032" ItemStyle-ForeColor="#FFFFFF" HorizontalAlign="Center" ItemStyle-BorderColor="#FFFFFF" ItemStyle-BorderStyle="Solid" ItemStyle-BorderWidth="1px">
                <ItemTemplate>
                    <img src='http://cdn.dstv.com/supersport.img/videoimages/<%#Container.DataItem("image")%>' width="80" height="60" align="left" style="border-right: 5px solid #000032; " /><a href="video.aspx?id=<%#Container.DataItem("id")%>" style="color:#ffffff;"><span style="font-weight:bold;"><%#Container.DataItem("name")%></span></a><br /><%#Container.DataItem("description")%>
                </ItemTemplate>
            </asp:DataList>
<%--
        </td>
    </tr>
    <tr>
        <td>
--%>
        <br />
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="article_btm_nav_bg" align="center" style="color:#000000">
                <tr>
                    <td height="26" class="article_btm_nav">
                        <table width="97%" cellspacing="0" cellpadding="0" border="0" align="center" style="color:#000000">
                            <tr>
                                <td width="33%" align="left" style="color:#000000"><asp:Literal id="ltl_prev" runat="server" EnableViewState="False" /></td>
                                <td width="33%" align="center" style="color:#000000"><asp:Literal id="ltl_pages" runat="server" EnableViewState="False" /></td>
                                <td width="33%" align="right" style="color:#000000"><asp:Literal id="ltl_next" runat="server" EnableViewState="False" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
<%--
        </td>
    </tr>
</table>
--%>