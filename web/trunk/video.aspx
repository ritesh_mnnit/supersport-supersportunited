<%@ Page Language="VB" AutoEventWireup="false" CodeFile="video.aspx.vb" Inherits="video" MasterPageFile="master.master" %>
<%@ MasterType VirtualPath="master.master" %>
<asp:Content runat="server" ContentPlaceHolderId="ContentPlaceHolder1" Id="content1">

    <script type="text/javascript" src="common/video.js?v=1"></script>
    
    <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
    <tr>
        <td colspan="3" style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding-left:10px;'>VIDEO</td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#ffffff" style="padding-left: 2px;">
        <table cellspacing="0" cellpadding="0" border="0" align="left" width="480">
            <tr>
                <td width="200" valign="top" style="padding-top: 5px;text-align:left;">
                <div id="newstext">
                <font color="#000000"><strong><asp:Literal id="ltl_headline" runat="server" EnableViewState="False" /></strong></font><br />
                <br />
                <asp:Panel Id="pnl_length" runat="server" EnableViewState="False" Visible="True">
               <span style='font-weight:bold;'>Clip Length: </span><asp:Literal id="ltl_length" runat="server" EnableViewState="False" /><br />
                </asp:Panel>
                <span style='font-weight:bold;'>Descritpion:</span> <asp:Literal id="ltl_detail" runat="server" EnableViewState="False" />
                </div>
                </td>
                <td width="10" valign="top"></td>
                <td width="320" valign="top" style="padding-top: 8px;text-align:left;">
                    <script language="JavaScript">
					<!--
					    if ( navigator.appName == "Netscape" )
					    {
					        //-- This next line ensures that any plug-ins just installed are updated in the browser
					        //-- without quitting the browser.
					        navigator.plug-ins.refresh();
					        // We don't need the APPLET within IE
					        // ***Please note that if you do not need to script events, you can safely remove the next two lines
					        document.write("\x3C" + "applet MAYSCRIPT Code=NPDS.npDSEvtObsProxy.class")
					        document.writeln(" width=5 height=5 name=appObs\x3E \x3C/applet\x3E")
					    }
					//-->
					</script> 
		            <object id="MediaPlayer" width="320" height="265" classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112" standby="Loading Microsoft Windows Media Player components..." type="application/x-oleobject">
		              <param name="FileName" value="http://www.supersport.com/bb/asxgen.asp?file=<% =FileName %>">
		              <param name="ShowControls" value="1">
		              <param name="ShowDisplay" value="0">
		              <param name="ShowStatusBar" value="0">
		              <param name="TransparentAtStart" value="1">
		              <param name="AutoSize" value="1"><embed type="application/x-mplayer2" pluginspage="http://www.microsoft.com/windows/windowsmedia/download/" filename="http://www.supersport.com/bb/asxgen.asp?file=<% =FileName %>" src="http://www.supersport.com/bb/asxgen.asp?file=<% =FileName %>" Name="MediaPlayer" ShowControls="1" ShowDisplay="0" ShowStatusBar="0" width="320" height="240" TransParentAtStart="1">
		            </object>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
        </table>
        </td>
    </tr>

    <tr>
        <td colspan="3" height="4"></td>
    </tr>
    <tr>
        <td colspan="3" height="27">
            <asp:Literal id="archivemenu" runat="server" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#ffffff" style="padding-left: 2px;">
        <table cellspacing="0" cellpadding="0" border="0" align="center" width="100%">
            <tr>
                <td align="center" style="padding-top: 7px;">
                    <div>
                    <div style="position: relative; width: 490px; height: 350px;">
                        <asp:Literal id="ltl_archive" runat="server" EnableViewState="false" />
                    </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td><br /><br />
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td height="26" class="article_btm_nav">
                                <table width="97%" cellspacing="0" cellpadding="0" border="0" align="center">
                                    <tr>
                                        <td width="33%" align="left"><asp:Literal id="ltl_prev" runat="server" EnableViewState="False" /></td>
                                        <td width="33%" align="center"><asp:Literal id="ltl_pages" runat="server" EnableViewState="False" /></td>
                                        <td width="33%" align="right"><asp:Literal id="ltl_next" runat="server" EnableViewState="False" /></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    </table>
</asp:Content>
