
Partial Class search
    Inherits System.Web.UI.Page
    Private SiteId As Integer = ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Local As Boolean = False
    Public TournamentId As Integer = -1
    Public DatabaseId As Integer = -1
    Public TeamId As Integer = -1
    Dim Counter As Integer
    Dim Count As Integer
    Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call search()
    End Sub
    Sub Search()

        Dim Start As Integer = 1
        Dim Page As Integer = 1
        If Not Request("page") Is Nothing Then
            Page = Convert.ToInt32(Request("page"))
            If Request("page") <> "1" Then
                Start = (8 * Convert.ToInt32(Request("page"))) - 7
            End If
        End If

        Dim da As SqlDataAdapter = New SqlDataAdapter
        Dim ds As DataSet = New DataSet
        Dim Items As Boolean = True
        If Request.QueryString("searchtext") = "" Then
            SqlQuery = New SqlCommand("SELECT TOP 100 Id, CategoryName, ArticleDate, Headline, Blurb, SmallImage As Image, SmallImageAlt As ImageAlt FROM articlebreakdown WHERE (CategoryName  ='supersportunited') AND (Active = 1) ORDER BY ArticleDate DESC", DatabaseConn)
        Else
            SqlQuery = New SqlCommand("SELECT TOP 100 Id, CategoryName, ArticleDate, Headline, Blurb, SmallImage As Image, SmallImageAlt As ImageAlt FROM articlebreakdown WHERE (CategoryName  ='supersportunited') AND (Active = 1) AND ((Blurb LIKE '%" & Request.QueryString("searchtext") & "%') OR (Headline LIKE '%" & Request.QueryString("searchtext") & "%') OR (Body  Like '%" & Request.QueryString("searchtext") & "%')) ORDER BY ArticleDate DESC", DatabaseConn)
        End If
        da.SelectCommand = SqlQuery
        da.Fill(ds, "Articles")
        Dim Row As DataRow
        Dim I As Integer
        Dim Bg As Boolean = False
        ltlContent.Text &= "<table cellspacing='3' cellpadding='5' border='0' align='center' width='489px'>"
        For I = Start - 1 To Start + 8
            Row = ds.Tables("Articles").Rows(I)
            ltlContent.Text &= "<tr>"
            If Row.Item("Image") <> "" Then
                ltlContent.Text &= "<td width='45%' valign='top' style='border:1px solid #c0c0c0;text-align:left;padding-top: 5px;padding-bottom: 5px;padding-left:2px;padding-right:2px;' bgcolor=""#ffffff""><a href='articles.aspx?Id=" & Row.Item("Id") & "' ><img src='http://images.supersport.com/" & Row.Item("Image") & "' alt='" & Row.Item("ImageAlt") & "' align='left' style='border: 3px solid #ffffff;' width='80px' height='80px'/></a><a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#000000;font-weight:bold;'>" & Row.Item("Headline") & "</a><br />" & Row.Item("Blurb") & " <a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#000000;font-weight:bold;'>read more</a></td>"
            Else
                ltlContent.Text &= "<td width='45%' valign='top' style='border:1px solid #c0c0c0;text-align:left;padding-top: 5px;padding-bottom: 5px;padding-left:2px;padding-right:2px;' bgcolor=""#ffffff""><a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#000000;font-weight:bold;'>" & Row.Item("Headline") & "</a><br />" & Row.Item("Blurb") & " <a href='articles.aspx?Id=" & Row.Item("Id") & "' style='color:#000000;font-weight:bold;'>read more</a></td>"
            End If
        Next
        ltlContent.Text &= "</tr>"
        ltlContent.Text &= "</table>"

        If Page > 1 Then
            ltlContent.Text &= "<div style='padding-top: 7px; float: left; text-align: left; padding-left: 5px;padding-bottom:5px;'><a href='search.aspx?page=" & Page - 1 & "&searchtext=" & Request.QueryString("searchtext") & "' style='color:#000000;font-weight:bold;'><< Previous</a></div>"
        End If
        If Page < 10 Then
            ltlContent.Text &= "<div style='padding-top: 7px; float: right; text-align: right; padding-right: 5px;padding-bottom:5px;'><a href='search.aspx?page=" & Page + 1 & "&searchtext=" & Request.QueryString("searchtext") & "' style='color:#000000;font-weight:bold;'>Next >></a></div>"
        End If
    End Sub

    Sub Page_UnLoad(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class
