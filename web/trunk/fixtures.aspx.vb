Partial Class fixtures
    Inherits System.Web.UI.Page
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("SiteId")
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Sub Page_Load(ByVal sedner As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If
        Fixtures()
    End Sub
    Private Sub Fixtures()
        ltlFixtures.Text &= "<table width='492px' cellspacing='1' cellpadding='0' border='0' style='background-color:#ffffff;'>"
        ltlFixtures.Text &= "<tr>"
        ltlFixtures.Text &= "<td colspan='6' style='background-image:url(images/ArticleHeader.jpg);width:492px;height:27px;color:#ffffff;font-weight:bold;text-align:left;padding:0px 0px 0px 5px;'>FIXTURES</td>"
        ltlFixtures.Text &= "</tr>"
        Dim LeagueName As String = ""
        Dim CurrentMonth As String = ""
        Dim Id As String = ""
        If Not Request.QueryString("id") Is Nothing Then
            Id = Request.QueryString("id")
        End If

        If Id <> "" Then
            SqlQuery = New SqlCommand("Select   Distinct '' As Ordering,'' As Id,a.MatchDateTime, b.Name As HomeTeam, c.Name As AwayTeam,d.Name As League, e.Name As Venue,Year(a.MatchDateTime), Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay, a.Postponed ,a.Completed FROM Soccer.dbo.Matches a INNER JOIN Soccer.dbo.TeamsByLeague b ON b.Id = a.TeamAId INNER JOIN Soccer.dbo.TeamsByLeague c ON c.Id = a.TeamBId INNER JOIN Soccer.dbo.Leagues d ON d.Id = a.LeagueId INNER JOIN Soccer.dbo.VenuesByLeague e ON e.Id = a.VenueId WHERE (a.LeagueId = " & Id & ") And (a.Completed = 0) Order By Ordering Desc,League,Year(a.MatchDateTime) , Month(a.MatchDateTime),Day(a.MatchDateTime) ,MatchDateTime", DatabaseConn)
        Else
            'SqlQuery = New SqlCommand("Select HomeTeam, League, AwayTeam, Venue, MatchDateTime, League From Soccer.dbo.SuperSportUnited_Fixtures Order By MatchDateTime", DatabaseConn)
			SqlQuery = New SqlCommand("Select   Distinct '' As Ordering,'' As Id,a.MatchDateTime, b.Name As HomeTeam, c.Name As AwayTeam,d.Name As League, e.Name As Venue,Year(a.MatchDateTime), Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay, a.Postponed ,a.Completed FROM Soccer.dbo.Matches a INNER JOIN Soccer.dbo.TeamsByLeague b ON b.Id = a.TeamAId INNER JOIN Soccer.dbo.TeamsByLeague c ON c.Id = a.TeamBId INNER JOIN Soccer.dbo.Leagues d ON d.Id = a.LeagueId INNER JOIN Soccer.dbo.VenuesByLeague e ON e.Id = a.VenueId WHERE (b.ParentId = 37 OR c.ParentId = 37 OR  c.LeagueId = 155 OR  e.LeagueId = 155) AND (c.LeagueId <> 123 OR e.LeagueId <> 123) AND (d.Name <> '2006-07 Castle Premiership') AND (b.Name LIKE '%SuperSport%' OR c.Name LIKE '%SuperSport%')AND (a.Completed = 0)  UNION Select Distinct  (Case When  Competition = 794 Then 1 Else 0 End) As  Ordering,a.Id,a.MatchDateTime, a.HomeTeamName As HomeTeam,a.AwayTeamname As AwayTeam,b.Competition_Name As League,a.Venue,Year(a.MatchDateTime), Month(a.MatchDateTime),Day(a.MatchDateTime) As MatchDay,(Case When StatusId = 14 Then 1 Else 0 End) As Postponed,a.Result From Soccer.dbo.pa_Matches a INNER JOIN Soccer.dbo.pa_Competitions b ON b.Competition_Id = a.Competition Where ((a.Competition = 794) or (a.Competition = 880))  And (a.Result = 0) AND (a.HomeTeamId = 44725 OR a.AwayTeamId = 44725) AND (a.StatusId < 14) And (b.Running = 1) Order By Ordering Desc,League,Year(a.MatchDateTime) , Month(a.MatchDateTime),Day(a.MatchDateTime) ,MatchDateTime", DatabaseConn)
        End If
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        If RsRec.HasRows Then
            While RsRec.Read
                Dim MatchDateTime As DateTime = RsRec("MatchDateTime")

                If MatchDateTime.ToString("MMMM") <> CurrentMonth Then
                    ltlFixtures.Text &= "<tr>"
                    ltlFixtures.Text &= "<td colspan='6' class='fix' style='font-weight:bold;'>" & MatchDateTime.ToString("MMMM") & "</td>"
                    ltlFixtures.Text &= "</tr>"
                End If

                ltlFixtures.Text &= "<tr>"
                ltlFixtures.Text &= "<td width='15' align='center' class='fix'>" & Day(RsRec("MatchDateTime")) & "</td>"
                If MatchDateTime.Hour = 0 And MatchDateTime.Minute = 0 Then
                    ltlFixtures.Text &= "<td class='fix' align='center'></td>"
                Else
                    ltlFixtures.Text &= "<td class='fix' align='center'>" & MatchDateTime.ToString("HH':'mm") & "</td>"
                End If
                ltlFixtures.Text &= "<td class='fix'>" & RsRec("HomeTeam") & "</td>"
                'If RsRec("PostPoned") = True Then
                    'ltlFixtures.Text &= "<td width='20' align='center' class='fix'><b>P-P</b></td>"
                'Else
                    ltlFixtures.Text &= "<td width='20' align='center' class='fix'><b>v</b></td>"
                'End If
                ltlFixtures.Text &= "<td class='fix'>" & RsRec("AwayTeam") & "</td>"
                ltlFixtures.Text &= "<td class='fix'>" & RsRec("Venue") & "</td>"
                ltlFixtures.Text &= "</tr>"
                CurrentMonth = MatchDateTime.ToString("MMMM")

            End While
        Else
            ltlFixtures.Text &= "<tr><td class='fix' style='font-weight:bold;text-align:center'>There are no upcoming fixtures</td></tr>"
        End If
        RsRec.Close()
        ltlFixtures.Text &= "<tr><td><a href=""javascript:history.go(-1)"" style=""font-weight:bold;"">Back</a></td></tr>"
        ltlFixtures.Text &= "</table>"
    End Sub
    Private Sub Page_UnLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub
End Class
