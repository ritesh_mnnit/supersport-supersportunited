Partial Class _Default
    Inherits System.Web.UI.Page
    Private DatabaseConn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("connString").ConnectionString)
    Private SqlQuery As SqlCommand
    Private Print As Boolean = False
    Private SiteNumber As Integer = System.Configuration.ConfigurationManager.AppSettings("Site")
    Dim Count As Integer
    Public startPics(6) As String
    Public headbArr(6) As String
    Public blurbArr(6) As String
    Public picArr(6) As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DatabaseConn.Open()
        Call Features()
        Call Features2()
        Call Articles()
        Call MoreNews()
        Call YouthAcademy()
        Call TopArticles()
    End Sub
    
    Sub Features()
        SqlQuery = New SqlCommand("Select Content from ZoneSiteContent where (Site = 25 ) AND (Type = 'Features')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlFeatures.Text = "" & RsRec("Content") & ""
        End While
        RsRec.Close()
    End Sub
    
    Sub Features2()
        SqlQuery = New SqlCommand("Select Content from ZoneSiteContent where (Site = 25 ) AND (Type = 'Features2')", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        While RsRec.Read
            ltlFeatures2.Text = "" & RsRec("Content") & ""
        End While
        RsRec.Close()
    End Sub
	
    Sub TopArticles()
        Dim query As String = "Select Top 6 a.Id, a.Headline,a.Active, a.Blurb, a.SmallImage As Image1, a.LargeImage As Image2, a.Image3, a.SmallImageAlt As ImageAlt, b.ArticleDate From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = 25) And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc"
        Dim sqlquery As New SqlCommand(query, DatabaseConn)

        Dim dbtbl As SqlDataReader = sqlquery.ExecuteReader

        Dim cnt As Integer

        cnt = 1

        While dbtbl.Read
            headbArr(cnt) = "<a style='color:#750f17;font-weight:bold;' href='articles.aspx?Id=" & dbtbl("Id") & "'>" & dbtbl("Headline") & "</a>"
            blurbArr(cnt) = "<a style='color:#000000;' href='articles.aspx?Id=" & dbtbl("Id") & "'>" & dbtbl("Blurb") & "</a>"
            picArr(cnt) = dbtbl("Image3")
            startPics(cnt) = dbtbl("Image1")
            cnt = cnt + 1
        End While

        lit_articles.Text &= " <td><div id=""art_1"" onMouseOver=""changeStory('p_1');changeBlurb('s_1');"" style=""cursor:hand;""><img src=""http://images.supersport.com/" & startPics(1) & """ style='border:1px solid #ffffff;'/></div></td>"
        lit_articles.Text &= "<td rowspan=""3"">"
        lit_articles.Text &= "<div id=""p_1"" style=""display:block""><img src=""http://images.supersport.com/" & picArr(1) & """  style=""width:310px;height:260px;border-left:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:0px solid #ffffff;""/></div>"
        lit_articles.Text &= "<div id=""p_2"" style=""display:none""><img src=""http://images.supersport.com/" & picArr(2) & """  style=""width:310px;height:260px;border-left:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:0px solid #ffffff;""/></div>"
        lit_articles.Text &= "<div id=""p_3"" style=""display:none""><img src=""http://images.supersport.com/" & picArr(3) & """  style=""width:310px;height:260px;border-left:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:0px solid #ffffff;""/></div>"
        lit_articles.Text &= "<div id=""p_4"" style=""display:none""><img src=""http://images.supersport.com/" & picArr(4) & """  style=""width:310px;height:260px;border-left:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:0px solid #ffffff;""/></div>"
        lit_articles.Text &= "<div id=""p_5"" style=""display:none""><img src=""http://images.supersport.com/" & picArr(5) & """  style=""width:310px;height:260px;border-left:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:0px solid #ffffff;""/></div>"
        lit_articles.Text &= "<div id=""p_6"" style=""display:none""><img src=""http://images.supersport.com/" & picArr(6) & """  style=""width:310px;height:260px;border-left:1px solid #ffffff;border-right:1px solid #ffffff;border-bottom:0px solid #ffffff;""/></div>"
        lit_articles.Text &= "</td>"
        lit_articles.Text &= "<td><div id=""art_2"" onMouseOver=""changeStory('p_2');changeBlurb('s_2');"" style=""cursor:hand;""><img src=""http://images.supersport.com/" & startPics(2) & """ style='border:1px solid #ffffff;'/></div></td>"
        lit_articles.Text &= "</tr>"
        lit_articles.Text &= "<tr>"
        lit_articles.Text &= "<td><div id=""art_3"" onMouseOver=""changeStory('p_3');changeBlurb('s_3');"" style=""cursor:hand;""><img src=""http://images.supersport.com/" & startPics(3) & """ style='border:1px solid #ffffff;'/></div></td>"
        lit_articles.Text &= "<td><div id=""art_4"" onMouseOver=""changeStory('p_4');changeBlurb('s_4');"" style=""cursor:hand;""><img src=""http://images.supersport.com/" & startPics(4) & """ style='border:1px solid #ffffff;'/></div></td>"
        lit_articles.Text &= "</tr>"
        lit_articles.Text &= "<tr>"
        lit_articles.Text &= "<td><div id=""art_5"" onMouseOver=""changeStory('p_5');changeBlurb('s_5');"" style=""cursor:hand;""><img src=""http://images.supersport.com/" & startPics(5) & """  style='border:1px solid #ffffff;'/></div></td>"
        lit_articles.Text &= "<td><div id=""art_6"" onMouseOver=""changeStory('p_6');;changeBlurb('s_6');"" style=""cursor:hand;""><img src=""http://images.supersport.com/" & startPics(6) & """ style='border:1px solid #ffffff;'/></div></td>"
        lit_articles.Text &= "</tr>"
        lit_articles.Text &= "</table>"
        lit_articles.Text &= "<table width=""492px"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
        lit_articles.Text &= "<tr>"
        lit_articles.Text &= "<td style=""background-image:url(images/news-background-main.png);background-repeat:repeat-x;width:240px;height:91px;color:#000000;vertical-align:top;text-align:left;padding-left:5px;"" width=""252px"">"
        lit_articles.Text &= "<div id=""s_1"" style=""display:block"">" & UCase(headbArr(1)) & "<br>" & Left(blurbArr(1), 175) & "</div>"
        lit_articles.Text &= "<div id=""s_2"" style=""display:none"">" & UCase(headbArr(2)) & "<br>" & Left(blurbArr(2), 175) & "</div>"
        lit_articles.Text &= "<div id=""s_3"" style=""display:none"">" & UCase(headbArr(3)) & "<br>" & Left(blurbArr(3), 175) & "</div>"
        lit_articles.Text &= "<div id=""s_4"" style=""display:none"">" & UCase(headbArr(4)) & "<br>" & Left(blurbArr(4), 175) & "</div>"
        lit_articles.Text &= "<div id=""s_5"" style=""display:none"">" & UCase(headbArr(5)) & "<br>" & Left(blurbArr(5), 175) & "</div>"
        lit_articles.Text &= "<div id=""s_6"" style=""display:none"">" & UCase(headbArr(6)) & "<br>" & Left(blurbArr(6), 175) & "</div>"
        lit_articles.Text &= "</td>"

    End Sub
    Sub Articles()
        ltlHeadlines.Text &= "<div style='font-weight:bold;size:11px;color:#750f17;'>TOP NEWS STORIES</div>"
        SqlQuery = New SqlCommand("Select Top 5 a.Id, a.Headline, a.Blurb, a.SmallImage As Image1, a.LargeImage As Image2, a.Image3, a.SmallImageAlt As ImageAlt, b.ArticleDate From SuperSportZone.dbo.ZoneArticles a INNER JOIN SuperSportZone.dbo.ZoneArticleCategories b ON b.ArticleId = a.Id INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category Where (c.Site = " & SiteNumber & ") And (a.Active = 1) And (a.Utilised = 1) Order By b.ArticleLevel Desc, b.ArticleDate Desc, a.Id Desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        Count = 1
        While RsRec.Read
            If Count > 1 Then
                ltlHeadlines.Text &= "<a href='articles.aspx?Id=" & RsRec("Id") & "' style='color:#000000;font-weight:normal;'><div style='padding-bottom:4px'><img src='images/li.png' border='0' style='padding-right:3px;'>" & Left(RsRec("Headline"), 35) & "</div></a>"
            End If
            Count = Count + 1
        End While
        RsRec.Close()
    End Sub
    Sub MoreNews()
        SqlQuery = New SqlCommand("Select Top 2 a.Id As Id,a.Headline As Headline,a.Active,a.ArticleDate,a.body As Body,a.Blurb As Blurb ,a.SmallImage,d.Image3 As Image, b.*  from articlebreakdown a  Inner Join ZoneArticleCategories b ON b.ArticleId=a.Id  INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN ZoneArticles d ON d.Id=a.Id where (c.Site = 4)  AND (a.categoryName ='supersoccer/bafanabafana') AND (a.Active=1) Order by a.ArticleDate desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        ltlmorenews_1.Text &= "<table cellspacing='0' cellpadding='2' border='0' width='245px' height='160px'>"
        Count = 1
        While RsRec.Read
            If Count > 0 Then
                If RsRec("SmallImage") <> "" Then
                    ltlmorenews_1.Text &= "<tr><td style='height:80px;vertical-align:Top;background-color:#ffffff;background-image:url(images/News-panels-background.png);background-repeat:repeat-x;width:250px;text-align:left;height:80px;color:#000000;'><a href='articles.aspx?Id=" & RsRec("Id") & "'><img src='http://images.supersport.com/" & RsRec("SmallImage") & "'  align='left' width='80' height='80' border='0' style='padding-right:3px;'/></a><span style='font-weight:bold;size:11px;color:#750f17;'><a href='articles.aspx?Id=" & RsRec("Id") & "'  style='color:#750f17;font-weight:bold;'>" & UCase(RsRec("Headline")) & "</a></span><br/>" & Left(RsRec("Blurb"), 80) & "&nbsp;...</td></tr>"
                Else
                    ltlmorenews_1.Text &= "<tr><td style='height:80px;vertical-align:Top;background-color:#ffffff;background-image:url(images/News-panels-background.png);background-repeat:repeat-x;width:250px;text-align:left;height:80px;color:#000000;'><a href='articles.aspx?Id=" & RsRec("Id") & "' style='color:#750f17;font-weight:bold;'>" & UCase(RsRec("Headline")) & "</a><br/>" & Left(RsRec("Blurb"), 170) & "</td></tr>"
                End If
            End If
            Count = Count + 1
        End While
        RsRec.Close()
        ltlmorenews_1.Text &= "</table>"
    End Sub
    Sub YouthAcademy()
        SqlQuery = New SqlCommand("Select Top 2 a.Id As Id,a.Headline As Headline,a.Active,a.ArticleDate,a.body As Body,a.Blurb As Blurb ,a.SmallImage,d.Image3 As Image, b.*  from articlebreakdown a  Inner Join ZoneArticleCategories b ON b.ArticleId=a.Id  INNER JOIN SuperSportZone.dbo.ZoneCategories c ON c.Id = b.Category INNER JOIN ZoneArticles d ON d.Id=a.Id where (c.Site = " & SiteNumber & ")  AND (a.Active=1) AND (a.categoryName ='supersportunited/youthacademy') Order by a.ArticleDate desc", DatabaseConn)
        Dim RsRec As SqlDataReader = SqlQuery.ExecuteReader
        ltlyouth.Text &= "<table cellspacing='0' cellpadding='2' border='0' width='245px' height='160px'>"
        Count = 1
        While RsRec.Read
            If Count > 0 Then
                If RsRec("SmallImage") <> "" Then
                    ltlyouth.Text &= "<tr><td style='height:80px;background-color:#ffffff;background-image:url(images/News-panels-background.png);background-repeat:repeat-x;width:250px;text-align:left;height:80px;color:#000000;'><a href='articles.aspx?Id=" & RsRec("Id") & "'><img src='http://images.supersport.com/" & RsRec("SmallImage") & "'  align='left' width='80' height='80' border='0' style='padding-right:3px;'/></a><span style='font-weight:bold;size:11px;color:#750f17;'><a href='articles.aspx?Id=" & RsRec("Id") & "'  style='color:#750f17;font-weight:bold;'>" & UCase(RsRec("Headline")) & "</a></span><br/>" & Left(RsRec("Blurb"), 80) & "&nbsp;...</td></tr>"
                Else
                    ltlyouth.Text &= "<tr><td style='height:80px;vertical-align:Top;background-color:#ffffff;background-image:url(images/News-panels-background.png);background-repeat:repeat-x;width:250px;text-align:left;height:80px;color:#000000;'><a href='articles.aspx?Id=" & RsRec("Id") & "' style='color:#750f17;font-weight:bold;'>" & UCase(RsRec("Headline")) & "</a><br/>" & Left(RsRec("Blurb"), 170) & "</td></tr>"
                End If
            End If
            Count = Count + 1
        End While
        RsRec.Close()
        ltlyouth.Text &= "</table>"
    End Sub
    Private Sub PageUnload(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Unload
        DatabaseConn.Close()
    End Sub

End Class
