﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.DAL.Football;

namespace SuperSport_SuperSportUnited
{
    public partial class player : System.Web.UI.Page
    {
        private FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderPlayer();
        }

        private void RenderPlayer()
        {
            ucPlayerView.PlayerCentre = _FootballCentre;
            ucPlayerView.DataBind();
        }
    }
}