﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport_SuperSportUnited
{
    public partial class galleries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ucGalleries.SiteID = SiteConfig.SiteID;
            ucGalleries.DataBind();
        }
    }
}