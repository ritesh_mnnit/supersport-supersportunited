﻿<%@ Page Title="SuperSport United-News" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="SuperSport_SuperSportUnited.news" %>
<%@ Register src="~/MatchCentreUI/Mobi/News/GenericNewsBrowser.ascx" tagname="GenericNewsBrowser" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Polls/GenericFeaturedPoll.ascx" tagname="FeaturedPoll" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Galleries/GenericGalleriesBrowser.ascx" tagname="Galleries" tagprefix="uc" %>
<%@ Register src="~/MatchCentreUI/Mobi/Logs/GenericLogs.ascx" tagname="GenericLogs" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="LatestNewsHeader" class="header">Latest News</div>
    <div id="LatestNewsContent" class="home-Container">
        <uc:GenericNewsBrowser ID="ucNewsBrowser" runat="server" CssClass="news" Count="4" />
    </div>
    <asp:Panel ID="pnlPoll" runat="server">
        <div id="PollsHeader" class="header">Poll</div>
        <div id="PollsContent" class="home-Container">
            <form id="Form1" runat="server">
                <uc:FeaturedPoll ID="ucFeaturedPoll" runat="server" CssClass="poll" />
            </form>
        </div>
    </asp:Panel>
    <div id="GalleriesHeader" class="header">
        <asp:HyperLink ID="lnkGalleriesPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.GalleriesBrowseRoute, null) %>'>
            Latest Pics
            <asp:Image ID="imgGalleriesHeaderIcon" runat="server" AlternateText="Latest Pics" ImageUrl="~/images/header_arrow_s.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="GalleriesContent" class="home-Container">
        <uc:Galleries ID="ucGalleries" runat="server" CssClass="galleries-control" Count="1" />
        <br class="clear" />
        <asp:HyperLink ID="lnkGalleriesPage2" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.GalleriesBrowseRoute, null) %>'
        CssClass="home-MoreLink">
            ...galleries
        </asp:HyperLink>
        <br class="clear" />
    </div>
    <div id="LogHeader" class="header">
        <asp:HyperLink ID="lnkLogsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.LogsRoute, null) %>'>
            Log
            <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_s.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="LogContent" class="home-Container">
        <uc:GenericLogs ID="ucLogs" runat="server" CssClass="logs" CentroidTeamName="SuperSport United" Count="5" />
        <br class="clear" />
    </div>
    <div id="MatchesHeader" class="header">
        <asp:HyperLink ID="lnkMatchesPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.MatchesRoute, null) %>'>
            Fixtures and Results
            <asp:Image ID="imgMatchesHeaderIcon" runat="server" AlternateText="Fixtures" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
</asp:Content>
