﻿com.dstvo.gelf4net Readme
=========================
1. Intro
2. Suggested appender usage
3. Adding additional fields to log messages
4. Example Application
5. Example Configuration

1. Intro
========
This package enables logging of GELF formatted messages to disk, upd, amqp and zeromq outputs.
It combines and customizes code from these projects:
Core project code		https://github.com/jjchiw/gelf4net
ZeroMQ Transport		https://bitbucket.org/rgl/log4net.zeromq/src/f702dca73313/log4net.ZeroMQ/


2. Suggested appender usage
===========================
Developer PC:				Use RollingFileAppender to log to local disk
DEV/QA/PROD Windows server:	Use Gelf4NetAppender(UDP) to log to Logstash instance running on environment. Logstash will then forward log events to the appropriate outputs.
							For important logs, use File and UDP appenders combined (unless the log Files are aggregated automatically by nxlog)


3. Adding additional fields to log messages
===========================================
The code below will add the key-value pair {"myCustomValue" : "A custom string value"} to the GELF message:

	log4net.ThreadContext.Properties["myCustomValue"] = "A custom string value";
	log.Error("An Error");


4. Example Application
======================
	using System;
	using log4net;
	using log4net.Config;

	namespace TestLogging
	{
		class MyProgram
		{
		    //Define a static reference once
			private static readonly ILog log = LogManager.GetLogger(typeof(MyProgram));

			static void Main(string[] args)
			{
				XmlConfigurator.Configure();													//Necessary to initialize log4net configuration in app.config - ideally this should be done once in your global.asax file's Application_Start()
				log4net.ThreadContext.Properties["myCustomValue"] = "A custom string value";	//This key-value pair is added to both log entries below
				log.Info("Some info");
				log.Error(new Exception("An error"));											//Automatically adds Exception properties to the log event
			}
		}
	}

5. Example Configuration
========================
	<?xml version="1.0" encoding="utf-8"?>
	<configuration>
	  <configSections>
		<section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />
	  </configSections>
	  <log4net>
		<root>
		  <appender-ref ref="ConsoleAppender" />
		  <appender-ref ref="Gelf4NetRollingFileAppender" />
		  <appender-ref ref="ZeroMQ"/>
		  <appender-ref ref="GelfUDPAppender" />
		</root>

		<appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
		  <layout type="log4net.Layout.PatternLayout">
			<conversionPattern value="%message%newline" />
		  </layout>
		</appender>
    
	 <!--This appender logs Gelf formatted messages straight to disk-->
		<appender type="Esilog.Gelf4net.Appender.Gelf4NetRollingFileAppender,Esilog.Gelf4net" name="Gelf4NetRollingFileAppender">
		  <file value="c:\log\dstvo\myapp\myapp.log" />
		  <appendtofile value="true" />
		  <!--Eg. The 5th backup will be in log.5 - This minimizes rollover cost.-->
		  <CountDirection value="1" />
		  <!--Rolls on Date and Size-->
		  <rollingStyle value="Composite" />
		  <!--Only keep this number of rolled log files-->
		  <MaxSizeRollBackups value="100" />
		  <maximumFileSize value="4MB" />
		  <!--This is important to get around a well-known log4net bug - https://issues.apache.org/jira/browse/LOG4NET-82 -->
		  <datePattern value=".yyyy-MM-dd" />    
		  <staticLogFileName value="true" />
		  <layout type="log4net.Layout.PatternLayout">
			<conversionPattern value="%message%newline" />
		  </layout>
		  <!--All custom additional fields will be added to the log event-->
		  <param name="AdditionalFields" value="version:1.0" />
		  <!--Set true to include file and line number where log event was raised-->
		  <param name="IncludeLocationInformation" value="true" />
		  <!--This value is used to identify and group your logs-->
		  <param name="Facility" value="MyApp" />
		</appender>

		<!--This appender logs Gelf formatted messages directly to a GELF endpoint - use this if no disk persistence is required-->
		<appender name="GelfUDPAppender" type="Esilog.Gelf4net.Appender.Gelf4NetAppender, Esilog.Gelf4net">
		  <param name="GrayLogServerHost" value="197.80.203.195" />
		  <param name="Facility" value="MyApp" />
		  <param name="AdditionalFields" value="version:1.0" />
		  <layout type="log4net.Layout.PatternLayout">
			<param name="ConversionPattern" value="%m%n"/>
		  </layout>
		</appender>

		  <!--**** clrzmq is currently only compatible with x86 - removing support for now ****
    This appender logs Gelf formatted messages to a ZeroMQ endpoint, e.g. a Logstash instance running on localhost-->
    <!--
    <appender name="ZeroMQ" type="Esilog.Gelf4net.Appender.Gelf4NetZeroMQAppender, Esilog.Gelf4net">
      <Address>tcp://127.0.0.1:9995</Address>
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%m" />
      </layout>
    </appender>
    -->
   
	  </log4net>
	 </configuration>