﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.CMS.Models;
using SuperSport.MatchCentre.UI.Mobi.SocialNetwork;

namespace SuperSport_SuperSportUnited
{
    public partial class article : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (null != Page.RouteData.Values["headline"])
            {
                Page.Title = "SuperSport United -" + Page.RouteData.Values["headline"].ToString().Replace('_', ' ');
            }
            ucArticleView.DataBind();

            ucArticleMetaTags.OpenGraphMetaType = OpenGraphMetaType.Article;
            ucArticleMetaTags.DataBind();

            RenderTopNews();
        }

        private void RenderTopNews()
        {
            ucNewsBrowser.SiteID = SiteConfig.SiteID;
            ucNewsBrowser.DataBind();
        }
    }
}