﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericNavigationMenu.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Navigation.GenericNavigationMenu" EnableViewState="false" %>
<div class="<%=CssClass %>">
<asp:ListView ID="rptNavigationMenu" runat="server" 
        onitemdatabound="rptNavigationMenu_ItemDataBound">
    <LayoutTemplate>
        <span runat="server" id="itemPlaceholder" />
    </LayoutTemplate>
    <ItemTemplate>
        <asp:HyperLink ID="lnkMenuItem" runat="server" CssClass="ItemMenuItemLink">
        <%-- lnkMenuItem.CssClass will be appended with GenericNavigationMenu.CssClass property --%>
            <span id="spanItemMenuItem" runat="server" class="ItemMenuItem"><%# Eval("Label").ToString() %></span>
            <%-- spanItemMenuItem.CssClass will be appended with GenericNavigationMenu.CssClass property --%>
        </asp:HyperLink>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <asp:HyperLink ID="lnkMenuItem" runat="server" CssClass="AltItemMenuItemLink">
        <%-- lnkMenuItem.CssClass will be appended with GenericNavigationMenu.CssClass property --%>
            <span id="spanItemMenuItem" runat="server" class="AltItemMenuItem"><%# Eval("Label").ToString() %></span>
            <%-- spanItemMenuItem.CssClass will be appended with GenericNavigationMenu.CssClass property --%>
        </asp:HyperLink>
    </AlternatingItemTemplate>
</asp:ListView>
</div>