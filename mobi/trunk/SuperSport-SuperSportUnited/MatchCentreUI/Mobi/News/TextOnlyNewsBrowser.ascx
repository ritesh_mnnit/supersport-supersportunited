﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericNewsBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.News.GenericNewsBrowser" EnableViewState="false" %>
<asp:Repeater ID="rptNews" runat="server" 
    onitemdatabound="rptNews_ItemDataBound">
    <HeaderTemplate>
<div class="<%=CssClass %>">
    </HeaderTemplate>
    <ItemTemplate>
        <div class="<%=CssClass %>Item">
            <asp:HyperLink ID="lnkHeadline" runat="server" CssClass="ItemHeadlineLink">
            <%-- lnkHeadline.CssClass will be appended with GenericNewsBrowser.CssClass property --%>
                <span class="<%=CssClass %>ItemHeadline"><%# Eval("Headline")%></span>
            </asp:HyperLink>
            <br class="<%=CssClass %>ItemHeadlineBreak" />
            <asp:HyperLink ID="lnkBlurb" runat="server" CssClass="ItemBlurbLink">
            <%-- lnkBlurb.CssClass will be appended with GenericNewsBrowser.CssClass property --%>
                <span class="<%=CssClass %>ItemBlurb"><%# Eval("Blurb")%></span>
            </asp:HyperLink>
            <br class="<%=CssClass %>ItemBlurbBreak" style="clear: both;" />
            <asp:HyperLink ID="lnkItem" runat="server" CssClass="ItemLink">
            <%-- lnkItem.CssClass will be appended with GenericNewsBrowser.CssClass property --%>
                ...view article
            </asp:HyperLink>
            <br class="<%=CssClass %>ItemLinkBreak" style="clear: both;" />
        </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <div class="<%=CssClass %>AltItem">
            <asp:HyperLink ID="lnkHeadline" runat="server" CssClass="AltItemHeadlineLink">
            <%-- lnkHeadline.CssClass will be appended with GenericNewsBrowser.CssClass property --%>
                <span class="<%=CssClass %>AltItemHeadline"><%# Eval("Headline")%></span>
            </asp:HyperLink>
            <br class="<%=CssClass %>AltItemHeadlineBreak" />
            <asp:HyperLink ID="lnkBlurb" runat="server" CssClass="AltItemBlurbLink">
            <%-- lnkBlurb.CssClass will be appended with GenericNewsBrowser.CssClass property --%>
                <span class="<%=CssClass %>AltItemBlurb"><%# Eval("Blurb")%></span>
            </asp:HyperLink>
            <br class="<%=CssClass %>AltItemBlurbBreak" style="clear: both;" />
            <asp:HyperLink ID="lnkItem" runat="server" CssClass="AltItemLink">
            <%-- lnkItem.CssClass will be appended with GenericNewsBrowser.CssClass property --%>
                ...view article
            </asp:HyperLink>
            <br class="<%=CssClass %>AltItemLinkBreak" style="clear: both;" />
        </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
</div>
    </FooterTemplate>
</asp:Repeater>