﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericPhotoView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Galleries.GenericPhotoView" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <div class="<%=CssClass %>ImageContainer">
        <asp:Image ID="imgPic" runat="server" CssClass="Image" />
        <%-- imgPic.CssClass will be prepended with GenericPhotoView.CssClass property --%>
    </div>
    <br class="<%=CssClass %>ImageBreak" />
    <div id="divHeadline" runat="server" class="Headline">
        <asp:Literal ID="LiteralHeadline" runat="server"></asp:Literal>
    </div>
    <br id="brHeadline" runat="server" class="HeadlineBreak" />
    <div id="divBody" runat="server" class="Body">
        <asp:Literal ID="LiteralBody" runat="server"></asp:Literal>
    </div>
    <br id="brBody" runat="server" class="BodyBreak" />
    <span class="<%=CssClass %>GalleryLinkContainer">
        <asp:HyperLink ID="lnkGallery" runat="server" CssClass="GalleryLink">
        <%-- lnkGallery.CssClass will be prepended with GenericPhotoView.CssClass property --%>
        back to gallery &gt;
        </asp:HyperLink>
    </span>
</div>
