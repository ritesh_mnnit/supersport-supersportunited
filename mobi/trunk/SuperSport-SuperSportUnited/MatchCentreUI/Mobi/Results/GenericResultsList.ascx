﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericResultsList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Results.GenericResultsList" EnableViewState="false" %>
<div class="<%=CssClass%>">
<asp:Repeater ID="rptResults" runat="server"
    onitemdatabound="rptResults_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <div class="<%=CssClass%>Item">
        <span class="<%=CssClass%>ItemHomeTeam"><%# Eval("HomeTeam")%></span>
        <span class="<%=CssClass%>ItemVs">vs</span>
        <span class="<%=CssClass%>ItemAwayTeam"><%# Eval("AwayTeam")%></span>
        <span class="<%=CssClass%>ItemHomeTeamScore"><%# Eval("HomeTeamScore")%></span>
        <span class="<%=CssClass%>ItemDash">&ndash;</span>
        <span class="<%=CssClass%>ItemAwayTeamScore"><%# Eval("AwayTeamScore")%></span>
        <span class="<%=CssClass%>ItemLeague"><%# Eval("LeagueName")%></span>
        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="ItemReportLink" Visible="false">
            <asp:Literal ID="LiteralMatchReport" runat="server">Match Report</asp:Literal>
        </asp:HyperLink>
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <div class="<%=CssClass%>AltItem">
        <span class="<%=CssClass%>AltItemHomeTeam"><%# Eval("HomeTeam")%></span>
        <span class="<%=CssClass%>AltItemVs">vs</span>
        <span class="<%=CssClass%>AltItemAwayTeam"><%# Eval("AwayTeam")%></span>
        <span class="<%=CssClass%>AltItemHomeTeamScore"><%# Eval("HomeTeamScore")%></span>
        <span class="<%=CssClass%>AltItemDash">&ndash;</span>
        <span class="<%=CssClass%>AltItemAwayTeamScore"><%# Eval("AwayTeamScore")%></span>
        <span class="<%=CssClass%>AltItemLeague"><%# Eval("LeagueName")%></span>
        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="AltItemReportLink" Visible="false">
            <asp:Literal ID="LiteralMatchReport" runat="server">Match Report</asp:Literal>
        </asp:HyperLink>
    </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
    <span class="<%=CssClass%>EmptyMessage">
        <asp:Literal ID="LiteralEmptyMessage" runat="server" Visible="false">
            There are currently no results available.
        </asp:Literal>
    </span>
    </FooterTemplate>
</asp:Repeater>
</div>
