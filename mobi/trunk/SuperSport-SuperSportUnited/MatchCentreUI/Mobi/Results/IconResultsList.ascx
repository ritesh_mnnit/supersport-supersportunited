﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericResultsList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Results.GenericResultsList" EnableViewState="false" %>
<div class="<%=CssClass%>">
<asp:Repeater ID="rptResults" runat="server"
    onitemdatabound="rptResults_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <div class="<%=CssClass%>Item">
        <span class="<%=CssClass%>ItemHomeTeam">
        <img 
            src="<%=TeamIconURLBase %><%# (null != Eval("HomeTeamId") && 0 != Convert.ToInt32(Eval("HomeTeamId")) ? Eval("HomeTeamId") : SuperSport.MatchCentre.UI.Utils.TextUtils.StripWhiteSpace(Eval("HomeTeam").ToString())) %>.jpg" 
            alt="<%# Eval("HomeTeam")%>"
            class="<%=CssClass%>ItemHomeTeamImage" />
        </span>
        <span class="<%=CssClass%>ItemVs">vs</span>
        <span class="<%=CssClass%>ItemAwayTeam">
        <img 
            src="<%=TeamIconURLBase %><%# (null != Eval("AwayTeamId") && 0 != Convert.ToInt32(Eval("AwayTeamId")) ? Eval("AwayTeamId") : SuperSport.MatchCentre.UI.Utils.TextUtils.StripWhiteSpace(Eval("AwayTeam").ToString())) %>.jpg" 
            alt="<%# Eval("AwayTeam")%>"
            class="<%=CssClass%>ItemAwayTeamImage" />        
        </span>
        <br class="<%=CssClass%>ItemBreakTeams" />
        <span class="<%=CssClass%>ItemHomeTeamScore"><%# Eval("HomeTeamScore")%></span>
        <span class="<%=CssClass%>ItemDash">&ndash;</span>
        <span class="<%=CssClass%>ItemAwayTeamScore"><%# Eval("AwayTeamScore")%></span>
        <br class="<%=CssClass%>ItemBreakScores" />
        <span class="<%=CssClass%>ItemLeague"><%# Eval("LeagueName")%></span>
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <div class="<%=CssClass%>AltItem">
        <span class="<%=CssClass%>AltItemHomeTeam">
        <img 
            src="<%=TeamIconURLBase %><%# (null != Eval("HomeTeamId") && 0 != Convert.ToInt32(Eval("HomeTeamId")) ? Eval("HomeTeamId") : SuperSport.MatchCentre.UI.Utils.TextUtils.StripWhiteSpace(Eval("HomeTeam").ToString())) %>.jpg" 
            alt="<%# Eval("HomeTeam")%>"
            class="<%=CssClass%>AltItemHomeTeamImage" />
        </span>
        <span class="<%=CssClass%>AltItemVs">vs</span>
        <span class="<%=CssClass%>AltItemAwayTeam">
        <img 
            src="<%=TeamIconURLBase %><%# (null != Eval("AwayTeamId") && 0 != Convert.ToInt32(Eval("AwayTeamId")) ? Eval("AwayTeamId") : SuperSport.MatchCentre.UI.Utils.TextUtils.StripWhiteSpace(Eval("AwayTeam").ToString())) %>.jpg" 
            alt="<%# Eval("AwayTeam")%>"
            class="<%=CssClass%>AltItemAwayTeamImage" />        
        </span>
        <br class="<%=CssClass%>AltItemBreakTeams" />
        <span class="<%=CssClass%>AltItemHomeTeamScore"><%# Eval("HomeTeamScore")%></span>
        <span class="<%=CssClass%>AltItemDash">&ndash;</span>
        <span class="<%=CssClass%>AltItemAwayTeamScore"><%# Eval("AwayTeamScore")%></span>
        <br class="<%=CssClass%>AltItemBreakScores" />
        <span class="<%=CssClass%>AltItemLeague"><%# Eval("LeagueName")%></span>
    </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
    <span class="<%=CssClass%>EmptyMessage">
        <asp:Literal ID="LiteralEmptyMessage" runat="server" Visible="false">
            There are currently no results available.
        </asp:Literal>
    </span>
    </FooterTemplate>
</asp:Repeater>
</div>
