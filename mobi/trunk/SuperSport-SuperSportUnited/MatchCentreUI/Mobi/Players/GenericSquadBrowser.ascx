﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericSquadBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Players.GenericSquadBrowser" EnableViewState="false" %>
<div class="<%= CssClass %>">
<asp:ListView ID="rptPlayers" runat="server" 
        onitemdatabound="rptPlayers_ItemDataBound">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <ItemTemplate>
        <%# AddGroupHeader(Eval("Position").ToString()) %>
        <div class="<%# CssClass %>PlayerContainer">
            <asp:HyperLink ID="lnkPlayer" runat="server" CssClass="PlayerLink">
            <%-- lnkPlayer.CssClass will be appended with GenericSquadBrowser.CssClass property --%>
            <%# Eval("CombinedName").ToString() %>
            </asp:HyperLink>
        </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <%# AddGroupHeader(Eval("Position").ToString()) %>
        <div class="<%# CssClass %>AltPlayerContainer">
            <asp:HyperLink ID="lnkPlayer" runat="server" CssClass="AltPlayerLink">
            <%-- lnkPlayer.CssClass will be appended with GenericSquadBrowser.CssClass property --%>
            <%# Eval("CombinedName").ToString() %>
            </asp:HyperLink>
        </div>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
    There is currently no squad data available.
    </EmptyDataTemplate>
</asp:ListView>
</div>
