﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FootballMatchEventsTimeLine.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Matches.FootballMatchEventsTimeLine" EnableViewState="false" %>
<div class="<%=CssClass%>">
<div class="<%=CssClass %>ItemA">
    <div class="<%=CssClass %>TeamName"><%= RenderedMatchDetails.TeamAName %></div>
    <asp:ListView ID="rptEventsTeamA" runat="server">
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="<%#CssClass %>Item<%# TeamAEventCssClass(Container.DataItem) %>">
                <span class="<%#CssClass %>ItemTime <%#CssClass + (null != Eval("Player2DisplayName") && !string.IsNullOrEmpty(Eval("Player2DisplayName").ToString()) ? "ItemTwoPlayerEvent" : "") %>">
                    <%# Eval("Time") %><%# Convert.ToInt32(Eval("ExtendTime")) > 0 ? " +" + Eval("ExtendTime").ToString() : "" %>
                </span>
                <img src="<%# GetEventIcon(Container.DataItem) %>" style="<%# GetEventIconStyle(Container.DataItem) %>" class="<%#CssClass %>ItemIcon" alt="" />
                <span class="<%#CssClass %>ItemTeam">
                    <span class="<%#CssClass %>ItemTeamPlayer1"><%# (null != Eval("Player2Surname") && !string.IsNullOrEmpty(Eval("Player2Surname").ToString()) ? Eval("Player2Surname") : Eval("Player1Surname"))%><%# SuperSport.MatchCentre.UI.Mobi.Matches.FootballMatchEventConstants.OWN_GOAL_EVENT_ID == Convert.ToInt32(Eval("EventId")) ? " (OG)" : "" %></span>
                    <span class="<%#CssClass %>ItemTeamPlayer2"><%# (null != Eval("Player2Surname") && !string.IsNullOrEmpty(Eval("Player2Surname").ToString()) ? Eval("Player1Surname") : "")%></span>
                </span>
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>
<div class="<%=CssClass %>ItemB">
    <div class="<%=CssClass %>TeamName"><%= RenderedMatchDetails.TeamBName %></div>
    <asp:ListView ID="rptEventsTeamB" runat="server">
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="<%#CssClass %>Item<%# this.TeamBEventCssClass(Container.DataItem) %>">
                <span class="<%#CssClass %>ItemTime <%#CssClass + (null != Eval("Player2DisplayName") && !string.IsNullOrEmpty(Eval("Player2DisplayName").ToString()) ? "ItemTwoPlayerEvent" : "") %>">
                    <%# Eval("Time") %><%# Convert.ToInt32(Eval("ExtendTime")) > 0 ? " +" + Eval("ExtendTime").ToString() : "" %>
                </span>
                <img src="<%# GetEventIcon(Container.DataItem) %>" style="<%# GetEventIconStyle(Container.DataItem) %>" class="<%#CssClass %>ItemIcon" alt="" />
                <span class="<%#CssClass %>ItemTeam">
                    <span class="<%#CssClass %>ItemTeamPlayer1"><%# (null != Eval("Player2Surname") && !string.IsNullOrEmpty(Eval("Player2Surname").ToString()) ? Eval("Player2Surname") : Eval("Player1Surname"))%><%# SuperSport.MatchCentre.UI.Mobi.Matches.FootballMatchEventConstants.OWN_GOAL_EVENT_ID == Convert.ToInt32(Eval("EventId")) ? " (OG)" : "" %></span>
                    <span class="<%#CssClass %>ItemTeamPlayer2"><%# (null != Eval("Player2Surname") && !string.IsNullOrEmpty(Eval("Player2Surname").ToString()) ? Eval("Player1Surname") : "")%></span>
                </span>
            </div>
        </ItemTemplate>
    </asp:ListView>
</div>
</div>