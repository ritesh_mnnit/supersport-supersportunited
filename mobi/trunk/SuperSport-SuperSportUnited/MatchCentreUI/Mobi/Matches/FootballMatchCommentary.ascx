﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FootballMatchCommentary.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Matches.FootballMatchCommentary" EnableViewState="false" %>
<div class="<%=CssClass%>">
    <asp:ListView ID="rptCommentary" runat="server" 
        onitemdatabound="rptCommentary_ItemDataBound">
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <div class="<%#CssClass %>Item">
                <span class="<%#CssClass %>ItemTime"><%# Eval("Time") %><%# Convert.ToInt32(Eval("ExtendTime")) > 0 ? " +" + Eval("ExtendTime").ToString() : "" %></span>
                <asp:Image ID="imgGoal" runat="server" Visible="false" CssClass="ItemIcon" />
                <asp:Image ID="imgBooking" runat="server" Visible="false" CssClass="ItemIcon" />
                <asp:Image ID="imgDismissal" runat="server" Visible="false" CssClass="ItemIcon" />
                <asp:Image ID="imgSubstitution" runat="server" Visible="false" CssClass="ItemIcon" />
                <span class="<%#CssClass %>ItemComment"><span class="<%# GetCommentsCssClass(Container.DataItem) %>"><%# Eval("Comments")%></span></span>
            </div>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <div class="<%#CssClass %>AltItem">
                <span class="<%#CssClass %>AltItemTime"><%# Eval("Time") %><%# Convert.ToInt32(Eval("ExtendTime")) > 0 ? " +" + Eval("ExtendTime").ToString() : "" %></span>
                <asp:Image ID="imgGoal" runat="server" Visible="false" CssClass="ItemIcon" />
                <asp:Image ID="imgBooking" runat="server" Visible="false" CssClass="ItemIcon" />
                <asp:Image ID="imgDismissal" runat="server" Visible="false" CssClass="ItemIcon" />
                <asp:Image ID="imgSubstitution" runat="server" Visible="false" CssClass="ItemIcon" />
                <span class="<%#CssClass %>AltItemComment"><span class="<%# GetCommentsCssClass(Container.DataItem) %>"><%# Eval("Comments")%></span></span>
            </div>
        </AlternatingItemTemplate>
    </asp:ListView>
</div>