﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FootballMatchSummary.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Matches.FootballMatchSummary" %>
<div class="<%=CssClass%>">
    <div id="pnlVenue" runat="server" class="<%=CssClass%>Item">
        <div class="<%=CssClass%>Item">
            <span class="<%=CssClass%>ItemLabel">Venue:</span>        
            <span class="<%=CssClass%>ItemValue"><asp:Literal ID="LiteralVenue" runat="server"></asp:Literal></span>        
        </div>
    </div>
    <div id="pnlAttendance" runat="server" class="<%=CssClass%>Item">
        <div class="<%=CssClass%>Item">
            <span class="<%=CssClass%>ItemLabel">Attendance:</span>        
            <span class="<%=CssClass%>ItemValue"><asp:Literal ID="LiteralAttendance" runat="server"></asp:Literal></span>        
        </div>
    </div>
    <div id="pnlReferee" runat="server" class="<%=CssClass%>Item">
        <div class="<%=CssClass%>Item">
            <span class="<%=CssClass%>ItemLabel">Referee:</span>        
            <span class="<%=CssClass%>ItemValue"><asp:Literal ID="LiteralReferee" runat="server"></asp:Literal></span>        
        </div>
    </div>
</div>
