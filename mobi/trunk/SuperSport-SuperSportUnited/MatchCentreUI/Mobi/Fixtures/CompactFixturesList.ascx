﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFixturesList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Fixtures.GenericFixtureList" EnableViewState="false" %>
<div class="<%=CssClass%>">
<asp:Repeater ID="rptFixtures" runat="server" 
    onitemdatabound="rptFixtures_ItemDataBound">
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <div class="<%=CssClass%>Item">
        <span class="<%=CssClass%>ItemDate"><%# String.Format("{0:dd/MM/yy}",Eval("MatchDateTime"))%></span>
        <span class="<%=CssClass%>ItemMatch"><%# Eval("HomeTeam")%> vs <%# Eval("AwayTeam")%></span>
        <span class="<%=CssClass%>ItemLeagueName">(<%# Eval("LeagueName")%>)</span>
        <span><asp:Literal ID="ltrlChannel" runat="server"></asp:Literal></span>
        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="ItemLiveCommentaryLink" Visible="false">
        <%-- lnkLiveCommentary.CssClass will be prepended with GenericFixtureList.CssClass --%>
            <asp:Literal ID="LiteralLiveCommentary" runat="server">Live Commentary</asp:Literal>
        </asp:HyperLink>
        <br class="<%=CssClass%>ItemBreak" />
    </div>
    </ItemTemplate>
    <AlternatingItemTemplate>
    <div class="<%=CssClass%>AltItem">
        <span class="<%=CssClass%>AltItemDate"><%# String.Format("{0:dd/MM/yy}",Eval("MatchDateTime"))%></span>
        <span class="<%=CssClass%>AltItemMatch"><%# Eval("HomeTeam")%> vs <%# Eval("AwayTeam")%></span>
        <span class="<%=CssClass%>AltItemLeagueName">(<%# Eval("LeagueName")%>)</span>
        <span><asp:Literal ID="ltrlChannel" runat="server"></asp:Literal></span>
        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="AltItemLiveCommentaryLink" Visible="false">
        <%-- lnkLiveCommentary.CssClass will be prepended with GenericFixtureList.CssClass --%>
            <asp:Literal ID="LiteralLiveCommentary" runat="server">Live Commentary</asp:Literal>
        </asp:HyperLink>
        <br class="<%=CssClass%>AltItemBreak" />
    </div>
    </AlternatingItemTemplate>
    <SeparatorTemplate>
    </SeparatorTemplate>
    <FooterTemplate>
    <span class="<%=CssClass%>EmptyMessage">
        <asp:Literal ID="LiteralEmptyMessage" runat="server" Visible="false">
            There are currently no fixtures available.
        </asp:Literal>
    </span>
    </FooterTemplate>
</asp:Repeater>
</div>
