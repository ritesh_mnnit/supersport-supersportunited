﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericClubsList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Clubs.GenericClubsList" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-clubs-widget ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptClubs" runat="server" 
        onitemdatabound="rptClubs_ItemDataBound">
        <LayoutTemplate>
            <ul class="mc-ui-clubs-list">
                <li id="itemPlaceholder" runat="server"></li>
            </ul>
        </LayoutTemplate>
        <ItemTemplate>
            <li id="itemPlaceholder" runat="server" class="mc-ui-clubs-list-item">
                <asp:Label ID="spanClubIconContainer" runat="server" CssClass="mc-ui-club-icon-container">
                    <a class="mc-ui-club-icon-link" href="<%# GetClubViewUrl(Convert.ToInt32(Eval("ID"))) %>">
                        <asp:Image ID="imgClubIcon" runat="server" CssClass="mc-ui-club-icon" />
                    </a>
                </asp:Label>
                <asp:Label ID="spanClubName" runat="server" Text="" CssClass="mc-ui-club-name">
                    <a class="mc-ui-club-name-link" href="<%# GetClubViewUrl(Convert.ToInt32(Eval("ID"))) %>">
                        <asp:Literal ID="LiteralClubName" runat="server"></asp:Literal>
                    </a>
                </asp:Label>
            </li>
        </ItemTemplate>
    </asp:ListView>
</div>
