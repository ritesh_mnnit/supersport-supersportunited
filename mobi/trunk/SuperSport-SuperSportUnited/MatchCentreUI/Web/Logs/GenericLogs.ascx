﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericLogs.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Logs.GenericLogs" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-logs-widget mc-ui-compact ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptLog" runat="server" onitemdatabound="rptLog_ItemDataBound">
        <LayoutTemplate>
            <div class="mc-ui-widget-data-header">
                <span class="mc-ui-log-position">&nbsp;</span>
                <span class="mc-ui-log-team">Team</span>
                <span class="mc-ui-log-played">P</span>
                <span class="mc-ui-log-won">W</span>
                <span class="mc-ui-log-drew">D</span>
                <span class="mc-ui-log-lost">L</span>
                <span class="mc-ui-log-pts">Pts</span>
                <br class="mc-ui-log-header-break" />
            </div>
            <div id="itemPlaceholder" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div id="divLogItemContainer" class="mc-ui-log-item-container" runat="server">
                <span class="mc-ui-log-position"><%# Eval("Position")%></span>
                <span class="mc-ui-log-team"><%# Eval("Team")%></span>
                <span class="mc-ui-log-played"><%# Eval("Played")%></span>
                <span class="mc-ui-log-won"><%# Eval("Won")%></span>
                <span class="mc-ui-log-drew"><%# Eval("Drew")%></span>
                <span class="mc-ui-log-lost"><%# Eval("Lost")%></span>
                <span class="mc-ui-log-pts"><%# Eval("Points")%></span>
                <br class="mc-ui-log-item-break" />
            </div>
        </ItemTemplate>
        <ItemSeparatorTemplate>
        </ItemSeparatorTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no logs available.
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>