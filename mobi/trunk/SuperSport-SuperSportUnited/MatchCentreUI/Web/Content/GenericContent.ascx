﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericContent.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Content.GenericContent" EnableViewState="false" %>
<div  id="<%= ClientID %>" class="mc-ui-content-widget ui-widget ui-widget-content <%=CssClass %>">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="mc-ui-content-header">
        <asp:Literal ID="LiteralHeader" runat="server"></asp:Literal>
    </asp:Panel>
    <div class="mc-ui-content-body">
        <asp:Literal ID="LiteralContent" runat="server"></asp:Literal>
    </div>
</div>