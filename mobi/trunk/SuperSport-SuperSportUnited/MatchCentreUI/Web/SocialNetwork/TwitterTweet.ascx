﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TwitterTweet.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.SocialNetwork.TwitterTweet" EnableViewState="false" %>
<div  id="<%= ClientID %>" class="mc-ui-twitter-tweet-widget ui-widget ui-widget-content <%=CssClass %>">
    <a 
        href="<%= Href %>" 
        class="twitter-share-button" 
        data-count="<%= Layout %>">
            <%= Action %>
    </a>
    <br class="mc-ui-social-network-like-break" />
</div>