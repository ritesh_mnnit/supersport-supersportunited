﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericNavigationMenu.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Navigation.GenericNavigationMenu" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-navigation-widget ui-widget ui-widget-content <%= CssClass %>">
<asp:PlaceHolder ID="PlaceHolderNavMenu" runat="server"></asp:PlaceHolder>
</div>