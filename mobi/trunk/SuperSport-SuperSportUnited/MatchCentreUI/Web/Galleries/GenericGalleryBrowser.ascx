﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericGalleryBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Galleries.GenericGalleryBrowser" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-galleria-widget ui-widget ui-widget-content <%=CssClass %>">
<asp:ListView ID="rptGallery" runat="server" 
    onitemdatabound="rptGallery_ItemDataBound">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <ItemTemplate>
        <asp:HyperLink ID="lnkGalleryPic" runat="server" CssClass="mc-ui-gallery-item-image-link">
            <asp:Image ID="imgGalleryPic" runat="server" CssClass="mc-ui-gallery-item-image" />
        </asp:HyperLink>
    </ItemTemplate>
    <ItemSeparatorTemplate>
    </ItemSeparatorTemplate>
    <EmptyDataTemplate>
    </EmptyDataTemplate>
</asp:ListView>
</div>
