﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DetailedSquadBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Players.DetailedSquadBrowser" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-squad-widget ui-widget ui-widget-content <%= CssClass %>">
<asp:ListView ID="rptSquadPositionGroups" runat="server" 
        onitemdatabound="rptSquadPositionGroups_ItemDataBound">
    <LayoutTemplate>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </LayoutTemplate>
    <ItemTemplate>
        <h3 class="mc-ui-squad-position-header"><%# Container.DataItem %></h3>
        <div class="mc-ui-squad-position-group">
            <asp:ListView ID="rptSquadMembers" runat="server" 
                onitemdatabound="rptSquadMembers_ItemDataBound">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="mc-ui-squad-member-container">
                        <div class="mc-ui-squad-member-image-container">
                            <a class="mc-ui-squad-member-full-image-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                                <asp:Image ID="imgSquadMemberFullPic" runat="server" CssClass="mc-ui-squad-member-full-image" />
                            </a>
                        </div>
                        <div class="mc-ui-squad-member-text-container">
                            <a class="mc-ui-squad-member-image-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                                <asp:Image ID="imgSquadMemberThumbnailPic" runat="server" CssClass="mc-ui-squad-member-image" />
                            </a>
                            <br class="mc-ui-squad-member-image-break" />
                            <span class="mc-ui-squad-member">
                                <a class="mc-ui-squad-member-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                                    <%# Eval("CombinedName").ToString() %>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-name-break" />
                            <span class="mc-ui-squad-member-position">
                                <a class="mc-ui-squad-member-position-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                                    <%# Eval("Position").ToString()%>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-position-break" />
                            <span class="mc-ui-squad-member-date-of-birth">
                                <a class="mc-ui-squad-member-date-of-birth-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                            <%# String.Format("{0:" + DateOfBirthFormat + "}", Eval("DateOfBirth"))%>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-date-of-birth-break" />
                            <span class="mc-ui-squad-member-place-of-birth">
                                <a class="mc-ui-squad-member-place-of-birth-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                            <%# Eval("PlaceOfBirth").ToString()%>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-place-of-birth-break" />
                            <span class="mc-ui-squad-member-height">
                                <a class="mc-ui-squad-member-height-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                            <%# Eval("Height").ToString()%>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-height-break" />
                            <span class="mc-ui-squad-member-weight">
                                <a class="mc-ui-squad-member-weight-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                            <%# Eval("Weight").ToString()%>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-weight-break" />
                            <span class="mc-ui-squad-member-biography">
                                <a class="mc-ui-squad-member-biography-link" href="<%# GetSquadMemberViewUrl(Convert.ToInt32(Eval("PersonId")), Convert.ToInt32(Eval("TeamId"))) %>">
                                    <span class="mc-ui-squad-member-biography-ellipsis">
                                        <span class="ellipsis_text">
                                            <%# Eval("Biography").ToString()%>
                                        </span>
                                    </span>
                                    <span class="mc-ui-squad-member-biography-non-ellipsis">
                                        <%# Eval("Biography").ToString()%>
                                    </span>
                                </a>
                            </span>
                            <br class="mc-ui-squad-member-biography-break" />
                        </div>
                    </div>
                </ItemTemplate>
                <EmptyDataTemplate>
                There is currently no squad data available for this position.
                </EmptyDataTemplate>
            </asp:ListView>
        </div>
    </ItemTemplate>
    <EmptyDataTemplate>
    There is currently no squad data available.
    </EmptyDataTemplate>
</asp:ListView>
</div>

