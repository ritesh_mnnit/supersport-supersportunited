﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFixturesList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Fixtures.GenericFixturesList" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-fixtures-widget mc-ui-compact ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptFixtures" runat="server" 
        onitemdatabound="rptFixtures_ItemDataBound">
        <LayoutTemplate>
            <table>
                <tr id="trHeader" runat="server" class="mc-ui-fixture-item-header">
                    <th class="mc-ui-fixture-item-date-container">
                        Date
                    </th>
                    <th class="mc-ui-fixture-item-time-container">
                        Time
                    </th>
                    <th id="thFirstChannels" runat="server" class="mc-ui-fixture-item-first-channels">
                        Channels
                    </th>
                    <th class="mc-ui-fixture-item-league">
                        League
                    </th>
                    <th class="mc-ui-fixture-item-teams-container">
                        Teams                        
                    </th>
                    <th class="mc-ui-fixture-item-venue">
                        Stadium
                    </th>
                    <th id="thLastChannels" runat="server" class="mc-ui-fixture-item-last-channels">
                        Channels
                    </th>
                    <th class="mc-ui-fixture-item-live-commentary">
                        Live
                    </th>
                </tr>
                <tr id="itemPlaceholder" runat="server"></tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr id="itemPlaceholder" runat="server" class="mc-ui-fixture-item-container">
                <td class="mc-ui-fixture-item-date-container">
                    <span class="mc-ui-fixture-item-date-label">Date:</span>
                    <span class="mc-ui-fixture-item-date"><%# String.Format("{0:" + DisplayDateFormat + "}", Eval("MatchDateTime"))%></span>
                </td>
                <td class="mc-ui-fixture-item-time-container">
                    <span class="mc-ui-fixture-item-time-label">Time:</span>
                    <span class="mc-ui-fixture-item-time"><%# String.Format("{0:HH:mm}",Eval("MatchDateTime"))%></span>
                </td>
                <td id="spanFirstChannels" runat="server" class="mc-ui-fixture-item-first-channels">
                    <asp:ListView ID="rptFirstFixtureChannels" runat="server">
                        <LayoutTemplate>
                            <span id="itemPlaceholder" runat="server"></span>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <span id="itemPlaceholder" runat="server" class="mc-ui-fixture-item-channel"><%# Container.DataItem.ToString() %></span>
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td class="mc-ui-fixture-item-league"><%# Eval("LeagueName")%></td>
                <td class="mc-ui-fixture-item-teams-container">
                    <span class="mc-ui-fixture-item-hometeam">
                        <span class="mc-ui-fixture-item-hometeam-text">
                            <%# (this.IsDisplayShortNames ? Eval("HomeTeamShortName") : Eval("HomeTeam"))%>
                        </span>
                    </span>
                    <span class="mc-ui-fixture-item-vs">vs</span>
                    <span class="mc-ui-fixture-item-awayteam">
                        <span class="mc-ui-fixture-item-awayteam-text">
                            <%# (this.IsDisplayShortNames ? Eval("AwayTeamShortName") : Eval("AwayTeam"))%>
                        </span>
                    </span>
                </td>
                <td class="mc-ui-fixture-item-venue"><%# Eval("Location")%></td>
                <td id="spanLastChannels" runat="server" class="mc-ui-fixture-item-last-channels">
                    <asp:ListView ID="rptLastFixtureChannels" runat="server">
                        <LayoutTemplate>
                            <span id="itemPlaceholder" runat="server"></span>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <span id="itemPlaceholder" runat="server" class="mc-ui-fixture-item-channel"><%# Container.DataItem.ToString() %></span>
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td class="mc-ui-fixture-item-live-commentary">
                    <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="mc-ui-fixture-item-live-commentary-link" Visible="false">
                        Live
                    </asp:HyperLink>
                </td>
            </tr>
        </ItemTemplate>
        <ItemSeparatorTemplate>
        </ItemSeparatorTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no fixtures available.
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>
