﻿<%@ Page Title="SuperSport United-Players" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="player.aspx.cs" Inherits="SuperSport_SuperSportUnited.player" %>
<%@ Register src="~/MatchCentreUI/Mobi/Players/GenericPlayerView.ascx" tagname="PlayerView" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <uc:PlayerView ID="ucPlayerView" runat="server" CssClass="player" />
</asp:Content>
