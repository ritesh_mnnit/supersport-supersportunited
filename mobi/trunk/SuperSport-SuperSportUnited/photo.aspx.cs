﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.CMS.Models;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport_SuperSportUnited
{
    public partial class photo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set Gallery Title
            List<PhotoGallery> galleries = PhotoGallery.GetPhotoGalleries(SiteConfig.SiteID, "");
            string galleryName = (
                from PhotoGallery gallery in galleries
                where gallery.Id == Convert.ToInt32(Page.RouteData.Values["galleryid"])
                select gallery.Headline
                ).FirstOrDefault();

            if (!string.IsNullOrEmpty(galleryName))
            {
                LiteralGalleryName.Text = galleryName;
                Page.Title = "SuperSport United - " + galleryName;
            }

            ucPhotoView.DataBind();
        }
    }
}