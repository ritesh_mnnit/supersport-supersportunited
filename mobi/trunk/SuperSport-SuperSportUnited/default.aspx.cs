﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.Models;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.CMS.Models;

namespace SuperSport_SuperSportUnited
{
    public partial class _default : System.Web.UI.Page
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderTopNews();
            RenderFixtures();
            RenderResults();
            RenderLogs();
        }

        private void RenderTopNews()
        {
            ucNewsBrowser.SiteID = SiteConfig.SiteID;
            ucNewsBrowser.DataBind();
        }

        private void RenderFixtures()
        {
            ucFixturesList.FixturesCentre = _FootballCentre;
            ucFixturesList.SiteID = SiteConfig.SiteID;
            ucFixturesList.DataBind();
        }

        private void RenderResults()
        {
            ucResultsList.ResultsCentre = _FootballCentre;
            ucResultsList.SiteID = SiteConfig.SiteID;
            ucResultsList.DataBind();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }
    }
}