﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.MatchCentre.DAL.Football;

namespace SuperSport_SuperSportUnited
{
    public partial class results : System.Web.UI.Page
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderResults();
        }

        private void RenderResults()
        {
            ucResultsList.ResultsCentre = _FootballCentre;
            ucResultsList.SiteID = SiteConfig.SiteID;
            ucResultsList.DataBind();
        }
    }
}