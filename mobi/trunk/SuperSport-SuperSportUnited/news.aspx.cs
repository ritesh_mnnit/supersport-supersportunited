﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using SuperSport.MatchCentre.DAL.Football;

namespace SuperSport_SuperSportUnited
{
    public partial class news : System.Web.UI.Page
    {
        protected FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderTopNews();
            RenderFeaturedPoll();
            RenderGalleries();
            RenderLogs();
        }

        private void RenderTopNews()
        {
            ucNewsBrowser.SiteID = SiteConfig.SiteID;
            ucNewsBrowser.DataBind();
        }

        private void RenderFeaturedPoll()
        {
            if (!string.IsNullOrEmpty(SiteConfig.DefaultPollCategory))
            {
                ucFeaturedPoll.SiteID = SiteConfig.SiteID;
                ucFeaturedPoll.PollCategory = SiteConfig.DefaultPollCategory;
                ucFeaturedPoll.DataBind();
            }
            else
            {
                pnlPoll.Visible = false;
            }
        }

        private void RenderGalleries()
        {
            ucGalleries.SiteID = SiteConfig.SiteID;
            ucGalleries.DataBind();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }
    }
}