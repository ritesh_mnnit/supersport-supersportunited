﻿<%@ Page Title="SuperSport United-Fixtures" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="fixtures.aspx.cs" Inherits="SuperSport_SuperSportUnited.fixtures" %>
<%@ Register src="~/CustomMatchCentreUI/Mobi/Fixtures/CompactFixturesList.ascx" tagname="FixturesList" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="FixturesHeader" class="header">Fixtures</div>
    <div id="FixturesContent" class="home-Container">
        <uc:FixturesList ID="ucFixturesList" runat="server" CssClass="fixtures" Count="50" />
    </div>
    <div id="ResultsHeader" class="header">
        <asp:HyperLink ID="lnkResultsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.ResultsRoute, null) %>'>
            Full Results
            <asp:Image ID="imgResultsHeaderIcon" runat="server" AlternateText="Results" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="LogHeader" class="header">
        <asp:HyperLink ID="lnkLogsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.LogsRoute, null) %>'>
            Log
            <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
</asp:Content>
