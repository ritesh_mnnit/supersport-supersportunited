﻿<%@ Page Title="SuperSport United-Results" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="results.aspx.cs" Inherits="SuperSport_SuperSportUnited.results" %>
<%@ Register src="~/MatchCentreUI/Mobi/Results/CompactResultsList.ascx" tagname="ResultsList" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceHolder" runat="server">
    <div id="ResultsHeader" class="header">Results</div>
    <div id="ResultsContent" class="home-Container">
        <uc:ResultsList ID="ucResultsList" runat="server" CssClass="results" Count="50" />
    </div>
    <div id="FixturesHeader" class="header">
        <asp:HyperLink ID="lnkFixturesPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.FixturesRoute, null) %>'>
            Full Fixtures
            <asp:Image ID="imgFixturesHeaderIcon" runat="server" AlternateText="Fixtures" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
    <div id="LogHeader" class="header">
        <asp:HyperLink ID="lnkLogsPage" runat="server" 
        NavigateUrl='<%$ Code: GetRouteUrl(SuperSport.MatchCentre.UI.Utils.Mobi.RoutingHelper.LogsRoute, null) %>'>
            Log
            <asp:Image ID="imgLogHeaderIcon" runat="server" AlternateText="Log" ImageUrl="~/images/header_arrow_e.png" CssClass="headericonright_e" />
        </asp:HyperLink>
    </div>
</asp:Content>
