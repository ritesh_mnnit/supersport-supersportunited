﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.DAL.Football;
using SuperSport.MatchCentre.UI.Utils;

namespace SuperSport_SuperSportUnited
{
    public partial class matches : System.Web.UI.Page
    {
        private FootballCentre _FootballCentre = new FootballCentre();

        protected void Page_Load(object sender, EventArgs e)
        {
            RenderFixtures();
            RenderResults();
            RenderLogs();
        }

        private void RenderFixtures()
        {
            ucFixturesList.FixturesCentre = _FootballCentre;
            ucFixturesList.SiteID = SiteConfig.SiteID;
            ucFixturesList.DataBind();
        }

        private void RenderResults()
        {
            ucResultsList.ResultsCentre = _FootballCentre;
            ucResultsList.SiteID = SiteConfig.SiteID;
            ucResultsList.DataBind();
        }

        private void RenderLogs()
        {
            ucLogs.LogsCentre = _FootballCentre;
            ucLogs.Category = SiteConfig.DefaultCategory;
            ucLogs.DataBind();
        }
    }
}