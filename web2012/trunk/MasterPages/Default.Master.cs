﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;
using System.Configuration;

namespace SUFCWeb.MasterPages
{
    public partial class Default : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterStartupScript(
                this.GetType(),
                ClientID + "_tweet",
                JavaScriptHelper.WrapScriptTag(
                    JavaScriptHelper.WrapJQueryOnReady(
                        "$('#tweet').tweet({\n" +
                        "username: '" + ConfigurationManager.AppSettings["TwitterUserName"] + "',\n" +
                        "join_text: 'auto',\n" +
                        "avatar_size: 32,\n" +
                        "count: 3,\n" +
                        "auto_join_text_default: 'we said,',\n" +
                        "auto_join_text_ed: 'we',\n" +
                        "auto_join_text_ing: 'we were',\n" +
                        "auto_join_text_reply: 'we replied to',\n" +
                        "auto_join_text_url: 'we were checking out',\n" +
                        "loading_text: 'loading tweets...',\n" +
                        "template: '{avatar}{text}<br />{time}<span class=\"tweet_separator\">.</span>{reply_action}<span class=\"tweet_separator\">.</span>{retweet_action}<span class=\"tweet_separator\">.</span>{favorite_action}' \n" +
                        "});"
                    )
                )
            );
        }
    }
}