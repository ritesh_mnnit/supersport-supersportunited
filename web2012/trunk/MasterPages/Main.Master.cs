﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SuperSport.MatchCentre.UI.Utils;

namespace SUFCWeb.MasterPages
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.ClientScript.RegisterClientScriptInclude("_jquery_", ResolveUrl("https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"));
            Page.ClientScript.RegisterClientScriptInclude("_jquery-ui_", ResolveUrl("https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"));
            Page.ClientScript.RegisterClientScriptInclude("_three-dots_", ResolveUrl(Utils.CDNBaseURL + "MatchCentreUI/scripts/jquery.dotdotdot-1.3.0-packed.js"));
            Page.ClientScript.RegisterClientScriptInclude("_facebox_", ResolveUrl(Utils.CDNBaseURL + "MatchCentreUI/scripts/facebox/facebox.js"));
            //Page.ClientScript.RegisterClientScriptInclude("_galleria_", ResolveUrl(Utils.CDNBaseURL + "MatchCentreUI/scripts/galleria/galleria-1.2.5.min.js"));
            //Page.ClientScript.RegisterClientScriptInclude("_galleria_theme_", ResolveUrl(Utils.CDNBaseURL + "scripts/galleria/themes/swallows/galleria.swallows.min.js"));
            Page.ClientScript.RegisterClientScriptInclude("_swfobject_", "http://core.dstv.com/js/swfobject.js");
            Page.ClientScript.RegisterClientScriptInclude("_match-centre-ui_", ResolveUrl(Utils.CDNBaseURL + "MatchCentreUI/scripts/matchcentreui.js"));
            Page.ClientScript.RegisterClientScriptInclude("_facebook_", "http://connect.facebook.net/en_US/all.js#xfbml=1");
            Page.ClientScript.RegisterClientScriptInclude("_twitter_", "http://platform.twitter.com/widgets.js");
            Page.ClientScript.RegisterClientScriptInclude("_supersleight_", ResolveUrl(Utils.CDNBaseURL + "assets/scripts/supersleight/supersleight.plugin.js"));
            Page.ClientScript.RegisterClientScriptInclude("_tweet_", ResolveUrl(Utils.CDNBaseURL + "assets/scripts/tweet/jquery.tweet.js"));

            Page.ClientScript.RegisterStartupScript(
                this.GetType(),
                ClientID + "IE_PNG_Fix",
                JavaScriptHelper.WrapConditional(
                    JavaScriptHelper.WrapScriptTag(
                        JavaScriptHelper.WrapJQueryOnReady(
                            "$('body').supersleight();"
                        )
                    ),
                    "lte IE 6"
                )
            );


        }
    }
}