﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace SUFCWeb
{
    public class Utils
    {
        private static string cdnBaseUrl = string.Empty;

        public static string CDNBaseURL
        {
            get
            {
                if (string.IsNullOrEmpty(cdnBaseUrl) &&
                    !string.IsNullOrEmpty(ConfigurationManager.AppSettings["CDNBaseUrl"]))
                {
                    return ConfigurationManager.AppSettings["CDNBaseUrl"];
                }
                else if (!string.IsNullOrEmpty(cdnBaseUrl))
                {
                    return cdnBaseUrl;
                }
                else
                {
                    return "~/";
                }
            }
            set
            {
                cdnBaseUrl = value;
            }
        }
    }
}