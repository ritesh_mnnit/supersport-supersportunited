﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFeaturedPoll.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Polls.GenericFeaturedPoll" EnableViewState="false" %>
<div class="<%=CssClass %>">
    <asp:Panel ID="pnlPoll" runat="server" Visible="true">
        <asp:HiddenField ID="hPollID" runat="server" />
        <div class="<%=CssClass %>QuestionContainer">
            <asp:Label ID="lbQuestion" runat="server" Text="" CssClass="Question"></asp:Label>
            <%-- lbQuestion.CssClass will be prepended with GenericFeaturedPoll.CssClass --%>
        </div>
        <div class="<%=CssClass %>AnswersContainer">
        <asp:RadioButtonList ID="radAnswers" runat="server" Visible="false" RepeatLayout="UnorderedList" EnableViewState="false">
        </asp:RadioButtonList>
        <asp:ListView ID="rptAnswers" runat="server" Visible="false">
            <ItemTemplate>
                <div class="<%#CssClass %>ItemAnswer">
                    <%#Eval("Answer") + " (" + Math.Round(Convert.ToDouble(Eval("PercentageVotes")), 2) + " % votes)" %>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <div class="<%#CssClass %>AltItemAnswer">
                    <%#Eval("Answer") + " (" + Math.Round(Convert.ToDouble(Eval("PercentageVotes")), 2) + " % votes)" %>
                </div>
            </AlternatingItemTemplate>
            <EmptyDataTemplate>
            There is currently no featured poll.
            </EmptyDataTemplate>
        </asp:ListView>
        </div>
        <div class="<%=CssClass %>ButtonContainer">
            <asp:Button ID="btVote" runat="server" Text="" Visible="false" 
            onclick="btVote_Click" CssClass="VoteButton" />
            <%-- btVote.CssClass will be prepended with GenericFeaturedPoll.CssClass --%>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlNoPoll" Visible="false" runat="server">
        No poll available at this time.
    </asp:Panel>
</div>