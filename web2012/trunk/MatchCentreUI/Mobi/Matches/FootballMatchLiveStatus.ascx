﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FootballMatchLiveStatus.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Mobi.Matches.FootballMatchLiveStatus" %>
<div class="<%=CssClass%>">
<div class="<%=CssClass%>Score">
<div class="<%=CssClass %>TeamA">
    <div class="<%=CssClass %>TeamName"><%= RenderedMatchDetails.TeamAName %></div>
    <div class="<%=CssClass %>TeamScore">
        <%= RenderedMatchDetails.TeamAScore%>
        <span class="<%=CssClass %>TeamPenalties"><%= ((RenderedMatchDetails.TeamAPenalties > 0 || RenderedMatchDetails.TeamBPenalties > 0) ? "(" + RenderedMatchDetails.TeamAPenalties + ")" : "") %></span>
    </div>
</div>
<span class="<%=CssClass %>TeamVS">V</span>
<div class="<%=CssClass %>TeamB">
    <div class="<%=CssClass %>TeamName"><%= RenderedMatchDetails.TeamBName %></div>
    <div class="<%=CssClass %>TeamScore">
        <%= RenderedMatchDetails.TeamBScore%>
        <span class="<%=CssClass %>TeamPenalties"><%= ((RenderedMatchDetails.TeamAPenalties > 0 || RenderedMatchDetails.TeamBPenalties > 0) ? "(" + RenderedMatchDetails.TeamBPenalties + ")" : "") %></span>
    </div>
</div>
</div>
<div class="<%=CssClass%>Status">
    <%= LastCommentTime %> <%= RenderedMatchDetails.Status %>
</div>
</div>