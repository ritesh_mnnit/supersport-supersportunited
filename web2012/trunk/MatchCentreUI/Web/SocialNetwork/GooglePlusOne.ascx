﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GooglePlusOne.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.SocialNetwork.GooglePlusOne" EnableViewState="false" %>
<div  id="<%= ClientID %>" class="mc-ui-google-plusone-widget ui-widget ui-widget-content <%=CssClass %>">
    <g:plusone 
        size="<%= this.Size %>" 
        count="<%= this.IsDisplayCount.ToString().ToLower() %>" 
        href="<%= this.Href %>" 
        annotation="<%= this.Annotation %>" 
        width="<%= this.Width %>">
    </g:plusone>
    <br class="mc-ui-social-network-like-break" />
</div>