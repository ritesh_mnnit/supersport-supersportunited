﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFeaturedPoll.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Polls.GenericFeaturedPoll" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-poll-widget ui-widget ui-widget-content <%=CssClass %>">
    <asp:Panel ID="pnlPoll" runat="server" Visible="true">
        <asp:HiddenField ID="hPollID" runat="server" />
        <div class="mc-ui-poll-question-container">
            <asp:Label ID="lbQuestion" runat="server" Text="" CssClass="mc-ui-poll-question"></asp:Label>
        </div>
        <div class="mc-ui-poll-answer-container">
        <asp:RadioButtonList ID="radAnswers" runat="server" Visible="false" RepeatLayout="UnorderedList" EnableViewState="false" CssClass="mc-ui-poll-answer-list">
        </asp:RadioButtonList>
        <asp:ListView ID="rptAnswers" runat="server" Visible="false">
            <LayoutTemplate>
                <ul class="mc-ui-poll-answer-list">
                    <li id="itemPlaceholder" runat="server"></li>
                </ul>
            </LayoutTemplate>
            <ItemTemplate>
                <li runat="server">
                    <span class="mc-ui-poll-answer-text"><%#Eval("Answer") %></span>
                    <span class="mc-ui-poll-answer-votes"><%#"(" + Math.Round(Convert.ToDouble(Eval("PercentageVotes")), 2) + " % votes)" %></span>
                </li>
            </ItemTemplate>
            <EmptyDataTemplate>
                <span class="mc-ui-empty-message">
                    There is currently no featured poll.
                </span>
            </EmptyDataTemplate>
        </asp:ListView>
        </div>
        <div class="mc-ui-poll-button-container">
            <asp:Button ID="btVote" runat="server" Text="Vote" Visible="false" 
                CssClass="mc-ui-poll-vote-button" onclick="btVote_Click" />
            <asp:Button ID="btView" runat="server" Text="Results" Visible="false" 
                CssClass="mc-ui-poll-view-button" onclick="btView_Click" />
            <asp:Button ID="btReturnToVote" runat="server" Text="Vote" Visible="false" 
                CssClass="mc-ui-poll-return-button" onclick="btReturnToVote_Click" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlNoPoll" Visible="false" runat="server">
        <span class="mc-ui-empty-message">
            No poll available at this time.
        </span>
    </asp:Panel>
</div>