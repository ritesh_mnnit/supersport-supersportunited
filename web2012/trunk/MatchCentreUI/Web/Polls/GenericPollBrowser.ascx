﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericPollBrowser.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Polls.GenericPollBrowser" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-polls-widget ui-widget ui-widget-content <%= CssClass %>">
    <asp:ListView ID="rptPolls" runat="server" OnItemDataBound="rptPolls_ItemDataBound">
        <LayoutTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
        </LayoutTemplate>
        <ItemTemplate>
            <asp:PlaceHolder ID="itemPlaceholder" runat="server">
                <h3 class="mc-ui-poll-question-container">
                    <%# Eval("Question")%>
                </h3>       
                <div class="mc-ui-poll-answer-container">
                    <asp:ListView ID="rptAnswers" runat="server">
                        <LayoutTemplate>
                            <ul class="mc-ui-poll-answer-list">
                                <li id="itemPlaceholder" runat="server"></li>
                            </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li id="itemPlaceholder" runat="server">
                                <span class="mc-ui-poll-answer-text"><%#Eval("Answer") %></span>
                                <span class="mc-ui-poll-answer-votes"><%#"(" + Math.Round(Convert.ToDouble(Eval("PercentageVotes")), 2) + " % votes)" %></span>
                            </li>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <span class="mc-ui-empty-message">
                                There are currently no poll answers.
                            </span>
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <span class="mc-ui-poll-total-votes"><%# Eval("TotalVotes")%> votes</span>
                </div>
            </asp:PlaceHolder>
        </ItemTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no polls.
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>