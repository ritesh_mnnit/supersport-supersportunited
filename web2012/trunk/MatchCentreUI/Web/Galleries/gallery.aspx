﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gallery.aspx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Galleries.gallery" EnableViewState="false" %>
<%@ Register src="GenericGalleryBrowser.ascx" tagname="GalleryBrowser" tagprefix="uc" %>
<div>
    <form id="form1" runat="server">
        <uc:GalleryBrowser ID="ucGalleryBrowser" runat="server" CssClass="gallery-page" />
    </form>
</div>