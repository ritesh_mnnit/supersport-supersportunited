﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericFixturesList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Fixtures.GenericFixturesList" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-fixtures-widget mc-ui-compact ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptFixtures" runat="server" 
        onitemdatabound="rptFixtures_ItemDataBound">
        <LayoutTemplate>
            <div id="itemPlaceholder" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div id="itemPlaceholder" runat="server" class="mc-ui-fixture-item-container">
                <span class="mc-ui-fixture-item-datetime-container">
                    <span class="mc-ui-fixture-item-date-container">
                        <span class="mc-ui-fixture-item-date-label">Date:</span>
                        <span class="mc-ui-fixture-item-date"><%# String.Format("{0:" + DisplayDateFormat + "}", Eval("MatchDateTime"))%></span>
                    </span>
                    <span class="mc-ui-fixture-item-time-container">
                        <span class="mc-ui-fixture-item-time-label">Time:</span>
                        <span class="mc-ui-fixture-item-time"><%# String.Format("{0:HH:mm}",Eval("MatchDateTime"))%></span>
                    </span>
                </span>
                <br class="mc-ui-fixture-item-datetime-break" />
                <span class="mc-ui-fixture-item-league"><%# Eval("LeagueName")%></span>
                <br class="mc-ui-fixture-item-league-break" />
                <span class="mc-ui-fixture-item-teams-container">
                    <span class="mc-ui-fixture-item-hometeam">
                        <span class="mc-ui-fixture-item-hometeam-text">
                            <%# (this.IsDisplayShortNames ? Eval("HomeTeamShortName") : Eval("HomeTeam"))%>
                        </span>
                    </span>
                    <span class="mc-ui-fixture-item-vs">vs</span>
                    <span class="mc-ui-fixture-item-awayteam">
                        <span class="mc-ui-fixture-item-awayteam-text">
                            <%# (this.IsDisplayShortNames ? Eval("AwayTeamShortName") : Eval("AwayTeam"))%>
                        </span>
                    </span>
                </span>
                <br class="mc-ui-fixture-item-teams-break" />
                <span class="mc-ui-fixture-item-venue"><%# Eval("Location")%></span>
                <br class="mc-ui-fixture-item-venue-break" />
                <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="mc-ui-fixture-item-live-commentary-link" Visible="false">
                    Live
                </asp:HyperLink>
                <br class="mc-ui-fixture-item-break" />
            </div>
        </ItemTemplate>
        <ItemSeparatorTemplate>
        </ItemSeparatorTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no fixtures available.
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>
