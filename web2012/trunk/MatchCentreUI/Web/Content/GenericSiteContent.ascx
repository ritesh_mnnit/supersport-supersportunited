﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericSiteContent.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Content.GenericSiteContent" EnableViewState="false" %>
<div  id="<%= ClientID %>" class="mc-ui-site-content-widget ui-widget ui-widget-content <%=CssClass %>">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="mc-ui-site-content-header">
        <asp:Literal ID="LiteralSiteContentHeader" runat="server"></asp:Literal>
    </asp:Panel>
    <div class="mc-ui-site-content-body">
        <asp:Literal ID="LiteralSiteContent" runat="server"></asp:Literal>
    </div>
</div>