﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericResultsList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Results.GenericResultsList" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-results-widget mc-ui-compact ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptResults" runat="server" 
        onitemdatabound="rptResults_ItemDataBound">
        <LayoutTemplate>
            <table>
                <tr id="trHeader" runat="server" class="mc-ui-result-item-header">
                    <th class="mc-ui-result-item-date-container">
                        Date
                    </th>
                    <th class="mc-ui-result-item-time-container">
                        Time
                    </th>
                    <th class="mc-ui-result-item-league">
                        League
                    </th>
                    <th class="mc-ui-result-item-hometeam">
                        Home Team                       
                    </th>
                    <th class="mc-ui-result-item-scores-container">
                        Result                        
                    </th>
                    <th class="mc-ui-result-item-awayteam">
                        Away Team                        
                    </th>
                    <th class="mc-ui-result-item-venue">
                        Stadium
                    </th>
                    <th class="mc-ui-result-item-live-commentary">
                        Match Report
                    </th>
                </tr>
                <tr id="itemPlaceholder" runat="server"></tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr id="itemPlaceholder" runat="server" class="mc-ui-result-item-container">
                <td class="mc-ui-result-item-date-container">
                    <span class="mc-ui-result-item-date-label">Date:</span>
                    <span class="mc-ui-result-item-date"><%# String.Format("{0:" + DisplayDateFormat + "}", Eval("MatchDateTime"))%></span>
                </td>
                <td class="mc-ui-result-item-time-container">
                    <span class="mc-ui-result-item-time-label">Time:</span>
                    <span class="mc-ui-result-item-time"><%# String.Format("{0:HH:mm}",Eval("MatchDateTime"))%></span>
                </td>
                <td class="mc-ui-result-item-league"><%# Eval("LeagueName")%></td>
                <td class="mc-ui-result-item-hometeam">
                    <span class="mc-ui-result-item-hometeam-text">
                        <%# (this.IsDisplayShortNames ? Eval("HomeTeamShortName") : Eval("HomeTeam"))%>
                    </span>
                </td>
                <td class="mc-ui-result-item-team-scores-container">
                    <span class="mc-ui-result-item-home-team-score"><%# Eval("HomeTeamScore")%></span>
                    <span class="mc-ui-result-item-dash">&ndash;</span>
                    <span class="mc-ui-result-item-away-team-score"><%# Eval("AwayTeamScore")%></span>
                </td>
                <td class="mc-ui-result-item-awayteam">
                    <span class="mc-ui-result-item-awayteam-text">
                        <%# (this.IsDisplayShortNames ? Eval("AwayTeamShortName") : Eval("AwayTeam"))%>
                    </span>
                </td>
                <td class="mc-ui-result-item-venue"><%# Eval("Location")%></td>
                <td class="mc-ui-result-item-live-commentary">
                    <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="mc-ui-result-item-live-commentary-link" Visible="false">
                        <asp:Literal ID="LiteralMatchReport" runat="server">Match Report</asp:Literal>
                    </asp:HyperLink>
                </td>
            </tr>
        </ItemTemplate>
        <ItemSeparatorTemplate>
        </ItemSeparatorTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no results available.
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>
