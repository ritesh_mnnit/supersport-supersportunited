﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericSeasonalResultsList.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Results.GenericSeasonalResultsList" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-results-widget ui-widget ui-widget-content <%=CssClass%>">
    <asp:ListView ID="rptMatchMonths" runat="server" 
        onitemdatabound="rptMatchMonths_ItemDataBound">
        <LayoutTemplate>
            <div ID="itemPlaceholder" runat="server"></div>
        </LayoutTemplate>
        <ItemTemplate>
            <div id="itemPlaceholder" runat="server">
                <h3 class="mc-ui-matchmonth-header">
                    <span class="mc-ui-matchmonth-header-date-label">Month:</span>
                    <span class="mc-ui-matchmonth-header-date"><%# String.Format("{0:" + DisplayMonthFormat + "}", Container.DataItem) %></span>
                </h3>
                <asp:ListView ID="rptMatchDays" runat="server" 
                    onitemdatabound="rptMatchDays_ItemDataBound">
                    <LayoutTemplate>
                        <div ID="itemPlaceholder" runat="server"></div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div id="itemPlaceholder" runat="server">
                            <h4 class="mc-ui-matchday-header">
                                <span class="mc-ui-matchday-header-date-label">Date:</span>
                                <span class="mc-ui-matchday-header-date"><%# String.Format("{0:" + DisplayDateFormat + "}", Container.DataItem) %></span>
                            </h4>
                            <asp:ListView ID="rptResults" runat="server" 
                                onitemdatabound="rptResults_ItemDataBound">
                                <LayoutTemplate>
                                    <div id="itemPlaceholder" runat="server"></div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div id="itemPlaceholder" runat="server" class="mc-ui-result-item-container">
                                        <span class="mc-ui-result-item-datetime-container">
                                            <span class="mc-ui-result-item-date-container">
                                                <span class="mc-ui-result-item-date-label">Date:</span>
                                                <span class="mc-ui-result-item-date"><%# String.Format("{0:" + DisplayDateFormat + "}", Eval("MatchDateTime"))%></span>
                                            </span>
                                            <span class="mc-ui-result-item-time-container">
                                                <span class="mc-ui-result-item-time-label">Time:</span>
                                                <span class="mc-ui-result-item-time"><%# String.Format("{0:HH:mm}",Eval("MatchDateTime"))%></span>
                                            </span>
                                        </span>
                                        <br class="mc-ui-result-item-datetime-break" />
                                        <span class="mc-ui-result-item-teams-container">
                                            <span class="mc-ui-result-item-hometeam">
                                                <span class="mc-ui-result-item-hometeam-text">
                                                    <%# (this.IsDisplayShortNames ? Eval("HomeTeamShortName") : Eval("HomeTeam"))%>
                                                </span>
                                                <span class="mc-ui-result-item-hometeam-icon-container">
                                                    <img 
                                                        src="<%=TeamIconURLBase %><%# (null != Eval("HomeTeamId") && 0 != Convert.ToInt32(Eval("HomeTeamId")) ? Eval("HomeTeamId") : SuperSport.MatchCentre.UI.Utils.TextUtils.StripWhiteSpace(Eval("HomeTeam").ToString())) %><%# SuperSport.MatchCentre.UI.ImageFormatUtil.ToExtension(IconImageFormat) %>" 
                                                        alt="<%# Eval("HomeTeam")%>"
                                                        class="mc-ui-result-item-hometeam-icon" />
                                                </span>
                                            </span>
                                            <span class="mc-ui-result-item-team-scores-container">
                                                <span class="mc-ui-result-item-hometeam-score"><%# Eval("HomeTeamScore")%></span>
                                                <span class="mc-ui-result-item-dash">&ndash;</span>
                                                <span class="mc-ui-result-item-awayteam-score"><%# Eval("AwayTeamScore")%></span>
                                            </span>
                                            <span class="mc-ui-result-item-awayteam">
                                                <span class="mc-ui-result-item-awayteam-text">
                                                    <%# (this.IsDisplayShortNames ? Eval("AwayTeamShortName") : Eval("AwayTeam"))%>
                                                </span>
                                                <span class="mc-ui-result-item-awayteam-icon-container">
                                                    <img 
                                                        src="<%=TeamIconURLBase %><%# (null != Eval("AwayTeamId") && 0 != Convert.ToInt32(Eval("AwayTeamId")) ? Eval("AwayTeamId") : SuperSport.MatchCentre.UI.Utils.TextUtils.StripWhiteSpace(Eval("AwayTeam").ToString())) %><%# SuperSport.MatchCentre.UI.ImageFormatUtil.ToExtension(IconImageFormat) %>" 
                                                        alt="<%# Eval("AwayTeam")%>"
                                                        class="mc-ui-result-item-awayteam-icon" />        
                                                </span>
                                            </span>
                                        </span>
                                        <br class="mc-ui-result-item-teams-break" />
                                        <span class="mc-ui-result-item-league"><%# Eval("LeagueName")%></span>
                                        <br class="mc-ui-result-item-league-break" />
                                        <span class="mc-ui-result-item-venue"><%# Eval("Location")%></span>
                                        <br class="mc-ui-result-item-venue-break" />
                                        <asp:HyperLink ID="lnkLiveCommentary" runat="server" CssClass="mc-ui-result-item-live-commentary-link" Visible="false">
                                            <asp:Literal ID="LiteralMatchReport" runat="server">Report</asp:Literal>
                                        </asp:HyperLink>
                                        <br class="mc-ui-result-item-break" />
                                    </div>
                                </ItemTemplate>
                                <ItemSeparatorTemplate>
                                </ItemSeparatorTemplate>
                                <EmptyDataTemplate>
                                    <span class="mc-ui-empty-message">
                                        There are currently no results available.
                                    </span>
                                </EmptyDataTemplate>
                            </asp:ListView>
                        </div>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <span class="mc-ui-empty-message">
                            There are currently no results available.
                        </span>
                    </EmptyDataTemplate>
                </asp:ListView>
            </div>
        </ItemTemplate>
        <EmptyDataTemplate>
            <span class="mc-ui-empty-message">
                There are currently no fixtures available.
            </span>
        </EmptyDataTemplate>
    </asp:ListView>
</div>

