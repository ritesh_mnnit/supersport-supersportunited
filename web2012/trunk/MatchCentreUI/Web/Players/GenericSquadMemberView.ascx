﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GenericSquadMemberView.ascx.cs" Inherits="SuperSport.MatchCentre.UI.Web.Players.GenericSquadMemberView" EnableViewState="false" %>
<div id="<%= ClientID %>" class="mc-ui-squad-member-widget ui-widget ui-widget-content <%= CssClass %>">
    
    <div class="mc-ui-squad-member-profile-header">
        <asp:Literal ID="LiteralProfileHeader" runat="server"></asp:Literal>
    </div>
    <div class="mc-ui-squad-member-profile-container">
        <asp:Image ID="imgProfilePic" runat="server" CssClass="mc-ui-squad-member-profile-image" />
        <asp:Image ID="imgProfilePicSmall" runat="server" CssClass="mc-ui-squad-member-profile-image-small" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-combine-name">
            Name:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-combine-name">
            <asp:Literal ID="LiteralCombinedName" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-profile-combined-name-break" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-dob">
            Date of Birth:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-dob">
            <asp:Literal ID="LiteralDateOfBirth" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-dob" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-pob">
            Place of Birth:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-pob">
            <asp:Literal ID="LiteralPlaceOfBirth" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-pob" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-position">
            Position:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-position">
            <asp:Literal ID="LiteralPosition" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-position" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-age">
            Age:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-age">
            <asp:Literal ID="LiteralAge" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-age" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-height">
            Height:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-height">
            <asp:Literal ID="LiteralHeight" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-height" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-weight">
            Weight:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-weight">
            <asp:Literal ID="LiteralWeight" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-weight" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-prevclubs">
            Previous Clubs:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-prevclubs">
            <asp:Literal ID="LiteralPreviousClubs" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-prevclubs" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-stats">
            Stats:
        </span>
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-stats">
            <asp:Literal ID="LiteralStats" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-stats" />
        <span class="mc-ui-squad-member-profile-label mc-ui-squad-member-bio">
            Biography:
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-bio-pre" />
        <span class="mc-ui-squad-member-profile-value mc-ui-squad-member-bio">
            <asp:Literal ID="LiteralProfileInfo" runat="server"></asp:Literal>
        </span>
        <br class="mc-ui-squad-member-profile-attr-break mc-ui-squad-member-bio-post" />
        <br class="mc-ui-squad-member-body-break" />
    </div>
    <div class="mc-ui-squad-member-squad-link-container">
        <asp:HyperLink ID="lnkSquad" runat="server" CssClass="mc-ui-squad-member-squad-link">
            ...See Other <asp:Literal ID="LiteralSquadCategory" runat="server"></asp:Literal>
        </asp:HyperLink>
    </div>
</div>
