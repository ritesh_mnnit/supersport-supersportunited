﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using SuperSport.MatchCentre.UI.Utils.Web;
using System.Collections;

namespace SUFCWeb
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            #region -- Standard Routes --
            RoutingHelper.BuildFixturesRouting("fixtures/", "~/fixtures.aspx");
            RoutingHelper.BuildResultsRouting("results/", "~/results.aspx");
            RoutingHelper.BuildLogsRouting("logs/", "~/logs.aspx");
            RoutingHelper.BuildTopScorersRouting("goalscorers/", "~/goalscorers.aspx");
            RoutingHelper.BuildNewsArchiveBrowseRouting("news/archive/", "~/newsarchive.aspx");
            RoutingHelper.BuildNewsViewRouting("news/article/", "~/article.aspx");
            RoutingHelper.BuildNewsBrowseRouting("news/", "~/news.aspx");
            RoutingHelper.BuildColumnArticleViewRouting("column/", "~/column.aspx");
            RoutingHelper.BuildColumnsBrowseRouting("columns/", "~/columns.aspx");
            RoutingHelper.BuildPagedGalleriesRouting("galleries/", "~/galleries.aspx");
            RoutingHelper.BuildGalleriesRouting("galleries/", "~/galleries.aspx");
            RoutingHelper.BuildGalleryRouting("gallery/", SuperSport.MatchCentre.UI.Web.Galleries.GenericGalleriesBrowser.DefaultPhotoGalleryPagePath);
            RoutingHelper.BuildPhotoViewRouting("photo/", "~/photo.aspx");
            RoutingHelper.BuildSquadBrowseRouting("team/", "~/team.aspx");
            RoutingHelper.BuildSquadMemberRouting("team-member/", "~/teammember.aspx");
            RoutingHelper.BuildAlternativeSquadBrowseRouting("match-officials/", "~/matchofficials.aspx");
            RoutingHelper.BuildAlternativeSquadMemberRouting("match-official/", "~/matchofficial.aspx");
            RoutingHelper.BuildClubBrowseRouting("clubs/", "~/clubs.aspx");
            RoutingHelper.BuildClubRouting("club/", "~/club.aspx");
            RoutingHelper.BuildVenueBrowseRouting("stadiums/", "~/venues.aspx");
            RoutingHelper.BuildVenueRouting("stadium/", "~/venue.aspx");
            RoutingHelper.BuildContentViewRouting("content/", "~/content.aspx");
            RoutingHelper.BuildPagedVideoGalleryRouting("videos/", "~/videos.aspx");
            RoutingHelper.BuildVideoGalleryRouting("videos/", "~/videos.aspx");
            RoutingHelper.BuildVideoRouting("video/", SuperSport.MatchCentre.UI.Web.Videos.GenericGalleryBrowser.DefaultVideoPagePath);
            RoutingHelper.BuildPollsBrowseRouting("polls/", "~/polls.aspx");
            RoutingHelper.BuildSearchRouting("search/", "~/search.aspx");
            #endregion
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            string fullOrigionalpath = Request.RawUrl;

            // Mobile check
            var userAgent = "";
            if (Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                userAgent = Request.ServerVariables["HTTP_USER_AGENT"].Trim().ToLower();
            }

            bool mobile = IsMobileBrowser(userAgent);

            if (Request.QueryString["mobile"] != null)
            {
                mobile = true;
            }

            if (mobile)
            {
                var tmpUrl = "http://mobi.sufc.co.za" + fullOrigionalpath;
                Response.Redirect(tmpUrl);
            }
        }



        private static bool IsMobileBrowser(string userAgent)
        {
            bool mobile = false;

            var mobiles = new ArrayList { "cldc", "midp", "symbian", "up.link", "windows ce", "iphone", "mobile safari" };

            var iEnum = mobiles.GetEnumerator();
            while (iEnum.MoveNext())
            {
                if (userAgent.IndexOf(iEnum.Current.ToString(), StringComparison.Ordinal) >= 0)
                {
                    mobile = true;
                    break;
                }
            }

            return mobile;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}